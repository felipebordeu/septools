SepTools: A Separated variable toolbox

Author: Felipe Bordeu


1. DEPENDENCIES

BasicTools Library, from https://gitlab.com/drti/basic-tools

2. INSTALLATION
    
    No copilation is needed for the moment
    
3. TESTING INFRASTRUCTURE

    SepTools uses the BasicTools infrastructure for testing. Please read the BasicTools
    documentation for more information.
    
    to test the library
    
    python -m BasicTools.Helpers.Tests -e SepTools
    

