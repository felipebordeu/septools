# -*- coding: utf-8 -*-

from itertools import product
from scipy.sparse import spdiags
from SeparatedTools.SeparatedField import SeparatedField


class SeparatedOp(SeparatedField):
    def __init__(self):
        super(SeparatedOp,self).__init__()

    def Dot(self,sepvec):

        res = sepvec.CopyStructure()
        res.Allocate(sepvec.GetNumberOfModes()*self.GetNumberOfModes() )
        for p in range(self.GetNumberOfPartitions()):
            parK = self.GetPartition(p)
            parV = sepvec.GetPartition(p)
            par = res.GetPartition(p)
            cpt = 0
            for KKi, VVj in product(range(parK.GetNumberOfModes()), range(parV.GetNumberOfModes())):
                v = parK.GetOp(KKi).dot(parV.GetMode(VVj))
                par.SetMode(cpt,v)
                cpt += 1
        return res


class SeparatedImplicit_OpT_Op(SeparatedField):
    def __init__(self,op=None):
        self.op = op

    def Dot(self,sepvec):

        res = sepvec.CopyStructure()
        res.Allocate(sepvec.GetNumberOfModes()*(self.GetNumberOfModes()) )
        for p in range(self.GetNumberOfPartitions()):
            parK = self.op.GetPartition(p)
            parV = sepvec.GetPartition(p)
            par = res.GetPartition(p)
            cpt = 0
            for KKj in range(parK.GetNumberOfModes()):
                opt = parK.GetOp(KKj).transpose()
                for KKi, VVj in product(range(parK.GetNumberOfModes()), range(parV.GetNumberOfModes())):
                    v = parK.GetOp(KKi).dot(parV.GetMode(VVj))
                    par.SetMode(cpt,opt.dot(v))
                    cpt += 1
        return res

    def GetNumberOfPartitions(self):
        return self.op.GetNumberOfPartitions()

    def GetNumberOfModes(self):
        return self.op.GetNumberOfModes()**2

    def SetNumberOfPartitions(self,i):
        raise(Exception("Cant use Set... on implicit objects"))

    def GetPartition(self,i):
        from SeparatedTools.MultiModeOperator import  MultiModeOperatorTOperator
        return MultiModeOperatorTOperator(self.op.GetPartition(i))

class SeparatedDiagonalOpFromVecOperator(SeparatedOp):
    def __init__(self,sepvec):
        super().__init__()
        self.sepvec = sepvec
        self.nbpar = sepvec.GetNumberOfPartitions()
        self.nbdofs= [sepvec.GetPartition(x).GetNumberOfValuesPerMode() for x in range(self.nbpar) ]

    def GetNumberOfPartitions(self):
        return self.nbpar

    def GetNumberOfModes(self):
        return self.sepvec.GetNumberOfModes()

    def GetPartition(self,i):

        class DiagonalOpFromVecPartition():
            def __init__(self,nbdofs,partition):
                self.nbdofs = nbdofs
                self.par = partition

            def GetOp(self,i):
                return spdiags(self.par.GetMode(i), [0], self.nbdofs, self.nbdofs)

            def GetNumberOfModes(self):
                return self.par.GetNumberOfModes()

            def GetName(self):
                return "diagOp"


        return DiagonalOpFromVecPartition(self.nbdofs[i],self.sepvec.GetPartition(i))

class SeparatedImplicitIdentityOperator(SeparatedField):
    def __init__(self):
        self.nbpar = 0
        self.nbdofs = []

    def SetNumberOfPartitions(self,nbpar):
        self.nbpar =nbpar
        self.nbdofs = [0]*self.GetNumberOfPartitions()

    def GetNumberOfPartitions(self):
        return self.nbpar

    def GetNumberOfModes(self):
        return 1

    def SetSizeFromSepvec(self,sepvec):

        self.SetNumberOfPartitions(sepvec.GetNumberOfPartitions())

        for ipar in range(self.GetNumberOfPartitions()):
            self.nbdofs[ipar] =  sepvec.GetPartition(ipar).GetNumberOfValuesPerMode()

    def GetPartition(self,i):
        class IdentityPartition():
            def __init__(self,nbdofs):
                self.nbdofs = nbdofs

            def GetOp(self,i):
                assert i ==0
                from scipy.sparse import eye
                return eye(self.nbdofs)

            def GetName(self):
                return "eyeOp"

            def __str__(self):
                return "eye({})".format(self.nbdofs)


        return IdentityPartition(self.nbdofs[i])

    def Dot(self,sepvec):
        from itertools import product

        res = sepvec.View()
        for ipar in range(self.GetNumberOfPartitions()):
            res.GetPartition(ipar).copy()
        return res


def CheckIntegrity(GUI=False):
    import numpy as np
    from SeparatedTools.Tests import CreateTestSepField
    field = CreateTestSepField(2)

    from SeparatedTools.MultiModeOperator import MultiModeOperator
    obj = SeparatedOp()
    obj.SetNumberOfPartitions(2)

    for ipar in range(2):
        par = MultiModeOperator()
        par.SetNumberOfModes(1)
        par.data[0] = np.eye(field.GetPartition(ipar).GetNumberOfValuesPerMode())
        obj.SetPartition(ipar,par)
    obj.GetNumberOfModes()

    obj.Dot(field)

############################### SeparatedImplicit_OpT_Op ###############################
    objTobj = SeparatedImplicit_OpT_Op(obj)
    objTobj.Dot(field)

    try:
        objTobj.SetNumberOfPartitions(2)
        raise RuntimeError() #pragma no cover
    except:
        pass

    objTobj.GetPartition(0)

############################### SeparatedImplicitIdentityOperator ##############################
    objI = SeparatedImplicitIdentityOperator()
    objI.SetNumberOfPartitions(2)

    print(objI.Dot(field) )

    objI.SetSizeFromSepvec(field)

    print(objI.GetPartition(0).GetOp(0))
    print(objI.GetName() )
    print(str(objI.GetPartition(0)) )

    objI.GetNumberOfModes()

############################### SeparatedDiagonalOpFromVecOperator ##############################
    objI = SeparatedDiagonalOpFromVecOperator(field)
    print(objI.GetPartition(0).GetName())

    return "ok"

if __name__ == '__main__':
    print(CheckIntegrity(True))# pragma no cover