import numpy as np

from BasicTools.Containers.Filters import ElementFilter
import BasicTools.Containers.ElementNames as EN

from BasicTools.FE.IntegrationsRules import IntegrationRulesAlmanac

from SeparatedTools.SeparatedField import SeparatedField
from SeparatedTools.MultiModeIPField import MultiModeIPField



def GetIPFieldFromFEField(inField,rule=None,ruleName=None,der=None,elementFilter=None):

    if rule is not None and ruleName is not None:
        raise(Exception("Cant provide rule and ruleName at the same time"))

    if rule is None:
        rules = [ IntegrationRulesAlmanac[rn] for rn in ruleName]
    else:
        rules = rule

    if elementFilter is None:
        elementFilter = [ None]*inField.GetNumberOfPartitions()

    for ipar in range(inField.GetNumberOfPartitions()):
        par = inField.GetPartition(ipar)
        if not isinstance(par,MultiModeIPField):
            break

        if par.rule is not rule[ipar]:
            raise(Exception("Cant transfer integration point fields"))
    else:
        return inField

    if der is None:
        der = [-1]*inField.GetNumberOfPartitions()

    outField =  SeparatedField()
    outField.SetNumberOfPartitions(inField.GetNumberOfPartitions())

    for ipar in range(inField.GetNumberOfPartitions()):
        par = inField.GetPartition(ipar)
        rule = rules[ipar]
        if isinstance(par,MultiModeIPField):
            outField.SetPartition(ipar,par)
        else:
            outipar = TransferMMFEFieldToMMIPField(par,der = der[ipar], rule= rule,elementFilter=elementFilter[ipar])
            outipar.SetName(par.GetName())
            outField.SetPartition(ipar,outipar)

    return outField

def TransferMMFEFieldToMMIPField(inField,der=-1,ruleName=None,rule=None,elementFilter=None):
    if elementFilter is None:
        elementFilter = ElementFilter(mesh=inField.mesh)

    outField = MultiModeIPField(name=inField.name,mesh=inField.mesh,ruleName=ruleName,rule=rule)
    outField.Allocate(inField.GetNumberOfModes())

    from BasicTools.FE.Spaces.FESpaces import LagrangeSpaceGeo
    geospaces = LagrangeSpaceGeo

    mesh = inField.mesh
    for name, data, elids in elementFilter:
    #for name,elements in outField.mesh.elements.items():
        NnodeperEl = EN.numberOfNodes[name]

        geospace = geospaces[name]
        space = inField.space[name]
        rule = outField.GetRuleFor(name)
        nbip = len(rule[1])

        geospace.SetIntegrationRule(rule[0],rule[1] )
        space.SetIntegrationRule(rule[0],rule[1] )
        numbering = inField.numbering[name]

        outputHeavyData = outField.data[name]
        for elid in elids:
            xcoor = np.array([mesh.nodes[data.connectivity[elid,j]] for j in range(NnodeperEl)])

            for iip in range(nbip):

                if der == -1:
                    valN = space.valN[iip]
                else:
                    _Jack, _Jdet, Jinv = geospace.GetJackAndDetI(iip,xcoor)
                    valN = Jinv(space.valdphidxi[iip])[der]

                dofsids = numbering[elid]

                for imode in range( inField.GetNumberOfModes() ):
                    valField = inField.GetMode(imode)[dofsids]
                    val  = np.dot(valN, valField).T
                    outputHeavyData[imode,elid,iip] = val

    return outField

def CheckIntegrity(GUI=False):
    from SeparatedTools.Tests import CreateTestMultiModeField

    T_xy = CreateTestMultiModeField("t",2)
    T_xy.Allocate(1)
    data = T_xy.GetMode(0)
    data = T_xy.mesh.nodes[:,0]
    T_xy.SetMode(0,data)

    T_z = CreateTestMultiModeField("t",1)
    T_z.Allocate(1)
    data = T_z.GetMode(0)
    data = T_z.mesh.nodes[:,0]
    T_z.SetMode(0,data)

    obj = SeparatedField()
    obj.SetNumberOfPartitions(2)
    obj.SetPartition(0,T_xy)
    obj.SetPartition(1,T_z)

    res = GetIPFieldFromFEField(obj,ruleName=["LagrangeIsoParam","LagrangeIsoParam"])

    print(obj)
    print(res)
    print(obj.GetPartition(1).GetHeavyData())
    print(res.GetPartition(1).GetHeavyData()["bar2"])

    return 'OK'

if __name__ == '__main__':
    print(CheckIntegrity(True))# pragma: no cover