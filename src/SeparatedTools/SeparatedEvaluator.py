import numpy as np

class FieldExtractor():
    def __init__(self,partition):
        self.partition = partition

    def GetNumberOfModes(self):
        return self.partition.GetNumberOfModes()


class FieldInterpolator(FieldExtractor):
    def __init__(self,partition):
        super().__init__(partition)
        self.coordinatesValues = None

    def GetNumberOfValuesPerMode(self):
        return self.coordinatesValues.shape(0)

    def GetMode(self,i):
        if self.op is None:
            raise RuntimeError('Must supply SourcePartition and TargetMesh before')
        return self.op.dot(self.partition.GetMode(i))

    def SetTargetMesh(self,mesh):
        self.mesh = mesh
        self.coordinatesValues = self.mesh.nodes
        self.Update()

    def SetSourcePartition(self,par):
        self.partition = par
        self.Update()

    def Update(self):
        if self.partition is None or self.mesh is None:
            self.op = None
            return

        from BasicTools.Containers.UnstructuredMeshFieldOperations import GetFieldTransferOp
        op = GetFieldTransferOp(self.partition,targetPoints=self.coordinatesValues)
        self.op = op


class FieldIndexExtractor(FieldExtractor):
    def __init__(self,partition):
        super().__init__(partition)
        self.indicesValues = []

    def GetNumberOfValuesPerMode(self):
        return len(self.indicesValues)

    def GetMode(self,i):
        return self.partition.GetMode(i)[self.indicesValues]


"""
class SeparatedFieldEvaluator():
    def __init__(self):
        self.coordinatesValues = {}

    def SetUseIndices(self,val=True):
        if self.useIndices == val:
            return

        if val:
            self.ClearCoordinatesValues()
        else:
            self.ClearCoordinatesIndices()
        self.useIndices =  val

    def ClearCoordinatesValues(self):
        self.coordinatesValues = {}

    def ClearCoordinatesIndices(self):
        self.indicesValues = {}

    def SetCoordinateValues(self,name,val):
        if self.useIndices:
            raise(Exception("Unsing indices for recontruction please set SetUseIndices(False)"))

        self.fixCoordinates[str(name)] = val

    def SetCoordinateValuesIndices(self,name,val):
        self.indicesValues[name] =  val

    def AddField(self,sepfield):
        self.fieldsToTreat.append(sepfield)

    def Compute(self):


        res = {}

        for f in fields:
            pass
"""
def ReconstructFullField(field):
    nbpar = field.GetNumberOfPartitions()
    args  =[]
    for i in range(nbpar):
        args.append(field.GetPartition(i).GetHeavyData())
        args.append([0,i+1])
    args.append( list(range(nbpar,0,-1) ) )
    return np.einsum(*args).flatten()

def MeshTensorProd(meshes,fields=None):
    if fields is None:
        fields = []

    nbm = len(meshes)
    recdim = sum([ m.props["ParafacDims"] for m in meshes])
    if recdim > 3:
        raise(Exception("Cant build a mesh of more than 3 Dimensions"))

    from BasicTools.Containers.UnstructuredMesh import UnstructuredMesh
    res = UnstructuredMesh()

    from SeparatedTools.SeparatedField import SeparatedField
    from SeparatedTools.MultiModeFEField import MultiModeFEField
    #building coordinates
    xcoords = [SeparatedField() for x in range(3)]

    names = ["z", "y", "z"]

    from BasicTools.FE.FETools import PrepareFEComputation

    feData = []
    for mesh in meshes:
        feData.append(PrepareFEComputation(mesh))

    import SeparatedTools.FieldNames as FN
    for x, name in enumerate([FN.MapX, FN.MapY, FN.MapZ]):
        if name in [f.name in fields ] :
            xcoords[cpt] = fields[ [f.name in fields ].index(name) ]
            continue

        xcoords[x].SetNumberOfPartitions(nbm)
        for ipar, mesh in enumerate(meshes):
            #vals = mesh.nodes[:,x]
            par = MultiModeFEField(names[x],mesh=mesh,space=feData[ipar][0],numbering=feData[ipar][1][0])
            xcoords[x].SetPartition(ipar,par)
        xcoords[x].Allocate(1,val=1)

        if x == 0:
            xcoords[0].GetPartition(0).SetMode(0,meshes[0].nodes[:,0])
        elif x == 1:
            if  meshes[0].props["ParafacDims"]  > 1 :
                xcoords[1].GetPartition(0).SetMode(0,meshes[0].nodes[:,1])
            else :
                xcoords[1].GetPartition(1).SetMode(0,meshes[1].nodes[:,0])
        elif x == 2:
            if recdim == 2:
                xcoords[2].GetPartition(1).SetMode(0,0.)
            else:
                if meshes[0].props["ParafacDims"]+meshes[1].props["ParafacDims"]==2:
                    xcoords[2].GetPartition(2).SetMode(0,meshes[2].nodes[:,0]) # 1D X 1D X 1D
                elif  meshes[0].props["ParafacDims"] == 2 :
                    xcoords[2].GetPartition(1).SetMode(0,meshes[1].nodes[:,0])
                elif meshes[1].props["ParafacDims"] == 2:
                    xcoords[2].GetPartition(1).SetMode(0,meshes[1].nodes[:,1])


    import BasicTools.Containers.ElementNames as EN
    res.nodes = np.vstack( tuple(ReconstructFullField(x) for x in xcoords) ).T
    #print(res.nodes)

    tensorProdElements = dict()
    tensorProdElements[(EN.Point_1, EN.Point_1)] = [EN.Point_1, list(range(1)) ]
    tensorProdElements[(EN.Point_1, EN.Bar_2)] = [EN.Bar_2, list(range(2)) ]
    tensorProdElements[(EN.Point_1, EN.Bar_3)] = [EN.Bar_3, list(range(3)) ]
    tensorProdElements[(EN.Point_1, EN.Quadrangle_4)] = [EN.Quadrangle_4, list(range(4))  ]
    tensorProdElements[(EN.Point_1, EN.Quadrangle_8)] = [EN.Quadrangle_8, [0,1,4,3,2,7,5,6,8] ]
    tensorProdElements[(EN.Point_1, EN.Quadrangle_9)] = [EN.Quadrangle_9, [0,1,4,3,2,7,5,6,8] ]
    tensorProdElements[(EN.Point_1, EN.Triangle_3)] = [EN.Triangle_3, list(range(3))  ]
    tensorProdElements[(EN.Point_1, EN.Triangle_6)] = [EN.Triangle_6, list(range(6))  ]

    tensorProdElements[(EN.Bar_2,EN.Point_1)] = [EN.Bar_2, list(range(2)) ]
    tensorProdElements[(EN.Bar_2,EN.Bar_2)] = [EN.Quadrangle_4, [0,1,3,2] ]
    #tensorProdElements[(EN.Bar_2,EN.Bar_3)] = [None ]
    tensorProdElements[(EN.Bar_2, EN.Quadrangle_4)] = [EN.Hexaedron_8, [0,2,4,6,1,3,5,7] ]
    tensorProdElements[(EN.Bar_2, EN.Triangle_3)] = [EN.Wedge_6, list(range(6))  ]
    #tensorProdElements[(EN.Bar_2, EN.Triangle_6)] = [None  ]

    tensorProdElements[(EN.Bar_3,EN.Point_1)] = [EN.Bar_3, [0,1,2] ]
    #tensorProdElements[(EN.Bar_3,EN.Bar_2)] = [None]
    tensorProdElements[(EN.Bar_3,EN.Bar_3)] = [EN.Quadrangle_9, [0,1,4,3,2,7,5,6,8] ]
    # 3  5  4       3  6  2
    # 6  8  7   ->  7  8  5
    # 0  2  1       0  4  1
    #tensorProdElements[(EN.Bar_3,EN.Quadrangle_4)] = [None ]
    #tensorProdElements[(EN.Bar_3,EN.Quadrangle_8)] = [None ]
#    #tensorProdElements[(EN.Bar_3, EN.Quadrangle_9)] = [EN.Hexaedron_27, list(range(27))  ]
    #tensorProdElements[(EN.Bar_3, EN.Triangle_3)] = [  ]
#    #tensorProdElements[(EN.Bar_3, EN.Triangle_6)] = [EN.Wedge_18, [] ]


    tensorProdElements[(EN.Quadrangle_4, EN.Point_1)] = [EN.Quadrangle_4, list(range(4)) ]
    tensorProdElements[(EN.Quadrangle_4, EN.Bar_2)] = [EN.Hexaedron_8, list(range(8)) ]
    #tensorProdElements[(EN.Quadrangle_4, EN.Bar_3)] = [None]
    #tensorProdElements[(EN.Quadrangle_4, EN.Quadrangle_4)] = [None]
    #tensorProdElements[(EN.Quadrangle_4, EN.Quadrangle_8)] = [None]
    #tensorProdElements[(EN.Quadrangle_4, EN.Quadrangle_9)] = [None]
    #tensorProdElements[(EN.Quadrangle_4, EN.Triangle_3)] = [None]
    #tensorProdElements[(EN.Quadrangle_4, EN.Triangle_6)] = [None]

    tensorProdElements[(EN.Quadrangle_8, EN.Point_1)] = [EN.Quadrangle_8, list(range(8)) ]
    #tensorProdElements[(EN.Quadrangle_8, EN.Bar_2)] = [None ]
    #tensorProdElements[(EN.Quadrangle_8, EN.Bar_3)] = [None ]
    #tensorProdElements[(EN.Quadrangle_8, EN.Quadrangle_4)] = [None]
    #tensorProdElements[(EN.Quadrangle_8, EN.Quadrangle_8)] = [None]
    #tensorProdElements[(EN.Quadrangle_8, EN.Quadrangle_9)] = [None]
    #tensorProdElements[(EN.Quadrangle_8, EN.Triangle_3)] = [None]
    #tensorProdElements[(EN.Quadrangle_8, EN.Triangle_6)] = [None]

    tensorProdElements[(EN.Quadrangle_9, EN.Point_1)] = [EN.Quadrangle_9, list(range(9)) ]
    #tensorProdElements[(EN.Quadrangle_9, EN.Bar_2)] = [EN.Hexaedron_8, list(range(8)) ]
#    tensorProdElements[(EN.Quadrangle_9, EN.Bar_3)] = [EN.Hexaedron_27, [] ]
    #tensorProdElements[(EN.Quadrangle_9, EN.Quadrangle_4)] = [None]
    #tensorProdElements[(EN.Quadrangle_9, EN.Quadrangle_8)] = [None]
    #tensorProdElements[(EN.Quadrangle_9, EN.Quadrangle_9)] = [None]
    #tensorProdElements[(EN.Quadrangle_9, EN.Triangle_3)] = [None]
    #tensorProdElements[(EN.Quadrangle_9, EN.Triangle_6)] = [None]

    tensorProdElements[(EN.Triangle_3, EN.Point_1)] = [EN.Triangle_3, list(range(3)) ]
    tensorProdElements[(EN.Triangle_3, EN.Bar_2)] = [EN.Wedge_6, list(range(6)) ]
    #tensorProdElements[(EN.Triangle_3, EN.Bar_3)] = [ None ]
    #tensorProdElements[(EN.Triangle_3, EN.Quadrangle_4)] = [None  ]
    #tensorProdElements[(EN.Triangle_3, EN.Quadrangle_8)] = [None ]
    #tensorProdElements[(EN.Triangle_3, EN.Quadrangle_9)] = [None ]
    #tensorProdElements[(EN.Triangle_3, EN.Triangle_3)] = [None  ]
    #tensorProdElements[(EN.Triangle_3, EN.Triangle_6)] = [None  ]

    tensorProdElements[(EN.Triangle_6, EN.Point_1)] = [EN.Triangle_6, list(range(6)) ]
#    tensorProdElements[(EN.Triangle_6, EN.Bar_2)] = [EN.Wedge_12, [0,1,2,6,7,8,3,4,5,9,10,11] ]
    tensorProdElements[(EN.Triangle_6, EN.Bar_3)] = [EN.Wedge_18, [0,1,2,6,7,8,3,4,5,9,10,11,12,13,14,15,16,17] ]
    #tensorProdElements[(EN.Triangle_6, EN.Quadrangle_4)] = [None  ]
    #tensorProdElements[(EN.Triangle_6, EN.Quadrangle_8)] = [None ]
    #tensorProdElements[(EN.Triangle_6, EN.Quadrangle_9)] = [None ]
    #tensorProdElements[(EN.Triangle_6, EN.Triangle_3)] = [None  ]
    #tensorProdElements[(EN.Triangle_6, EN.Triangle_6)] = [None  ]


    from SeparatedTools.MiscTools import Expand
    #globalcpt = 0

#    def GetConnectivityInReconstructedDomain2D(conn1,conn2,nbnode1):
#        return res
#        return res
#        nbel1= conn1.shape[0]
#        res = np.empty( ( nbel1*conn2.shape[0], conn1.shape[1]*conn2.shape[1] ), dtype=int)
#        cpt1 =0

#        for n2 in range(conn2.shape[0]):
#            cpt2 = 0
#            for col2 in range(conn2.shape[2]):
#                res[cpt1:cpt1+nbel1,cpt2:cpt2+ncol1] = conn1 + conn2[,]*nbnode1
#                cpt2 = ncol1
#            cpt1 += nbel1
    nbnode1 = meshes[0].GetNumberOfNodes()

    if nbm == 2:
        for name1,data1 in meshes[0].elements.items():
            for name2, data2 in meshes[1].elements.items():
                if not (name1,name2) in tensorProdElements:
                    print("skeep prod of {} ".format( (name1,name2) ))
                    continue
                eltype, permut = tensorProdElements[(name1,name2)]
                print("Building {} X {}--------------------------------------".format(name1,name2))
                #print(eltype)
                #print(permut)
                data = res.GetElementsOfType(eltype)
                coon1 = data1.connectivity
                coon2 = data2.connectivity
                nbNewElem = data1.GetNumberOfElements()*data2.GetNumberOfElements()
                if nbNewElem == 0:
                    continue
                newElems = np.zeros( ( nbNewElem  ,len(permut)  ),dtype=int)
                for i in range(coon2.shape[0]):
                    for j in range(coon2.shape[1]):
                        newElems[i*coon1.shape[0]:(i+1)*coon1.shape[0] ,j*coon1.shape[1]:(j+1)*coon1.shape[1] ] = coon1+ coon2[i,j]*nbnode1
                #newElems = np.kron  (coon2*nbnode1,coon1)
                newElems = newElems[:,permut]
                data.Reserve(data.GetNumberOfElements()+nbNewElem)
                data.connectivity[data.GetNumberOfElements():data.GetNumberOfElements()+nbNewElem,:] = newElems
                data.cpt += nbNewElem
                #if name1 == EN.Point_1 and name2 == EN.Point_1:
                #    nbe1 = data1.GetNumberoOfElements()
                #    nbe2 = data2.GetNumberoOfElements()
                #    nbNewElem = nbe1*nbe2
                #    for i,j in Expand((nbe1,nbe2)):
                #        coon1 = data1.connectivity[i,:]
                #        coon2 = data2.connectivity[j,:]
                #        coontmp = GetConnectivityInReconstructedDomain2D(conn1,coon2,nbnode1)

                #        data.AddNewElement(get  )

    elif nbm == 3:
        res = MeshTensorProd([ meshes[1],meshes[2] ])
        res.props["ParafacDims"] = 2

        return MeshTensorProd([meshes[0],res])

    res.GenerateManufacturedOriginalIDs()
    res.PrepareForOutput()
    return res

def CheckIntegrity(GUI=False):
    from BasicTools.Containers.UnstructuredMesh import UnstructuredMesh as UnstructuredMesh
    import BasicTools.Containers.ElementNames as EN
    # ° lonely point    ° point element      °° bar-2    °°° bar_3
    def Create1DMesh(extra=0):
        res = UnstructuredMesh()
        npoints = 7 + extra
        points = np.zeros((npoints,3))
        points[:,0] = np.linspace(0,npoints-1,npoints)
        res.nodes = points
        elements = res.GetElementsOfType(EN.Point_1)
        elements.connectivity = np.array([[1]],dtype=np.int)
        elements.cpt = elements.connectivity.shape[0]

        elements = res.GetElementsOfType(EN.Bar_2)
        elements.connectivity = np.array([[2,3]],dtype=np.int)
        elements.cpt = elements.connectivity.shape[0]

        elements = res.GetElementsOfType(EN.Bar_3)
        elements.connectivity = np.array([[4,6,5]],dtype=np.int)
        elements.cpt = elements.connectivity.shape[0]

        res.GenerateManufacturedOriginalIDs()
        res.PrepareForOutput()
        res.props["ParafacDims"] = 1

        return res

    def Create2DMesh():
        res = UnstructuredMesh()
        points = np.array([[0.,0.,0.], #lonely node
                           [1.,1.,0],  # Point_1
                           [2,0,0],  # bar _2
                           [2,1,0],
                           [3,0,0],  # bar _3
                           [3,2,0],
                           [3,1,0],
                           [4,0,0],  # quad4
                           [5,0,0],
                           [5,1,0],
                           [4,1,0],
                           [6,0,0],  # quad8
                           [8,0,0],
                           [8,2,0],
                           [6,2,0],
                           [7,0,0],
                           [8,1,0],
                           [7,2,0],
                           [6,1,0],
                           [9,0,0],  # quad9
                           [11,0,0],
                           [11,2,0],
                           [9,2,0],
                           [10,0,0],
                           [11,1,0],
                           [10,2,0],
                           [9,1,0],
                           [10,1,0],
                           [12,0,0],  # tri3
                           [13,0,0],
                           [12.5,1,0],

                           [14,0,0], # tri 6
                           [16,0,0],
                           [15,1,0],
                           [15,0,0],
                           [15.5,0.5,0],
                           [14.5,0.5,0],


                           ])
        res.nodes = points

        def AddSingleElement(res,etype,number):
            elements = res.GetElementsOfType(etype)
            elements.connectivity = np.array([number],dtype=np.int)
            elements.cpt = elements.connectivity.shape[0]

        AddSingleElement(res,EN.Point_1,[1])
        AddSingleElement(res,EN.Bar_2,[2,3])
        AddSingleElement(res,EN.Bar_3,[4,5,6])
        AddSingleElement(res,EN.Quadrangle_4,[7,8,9,10])
        AddSingleElement(res,EN.Quadrangle_8,[11,12,13,14,15,16,17,18])
        AddSingleElement(res,EN.Quadrangle_9,[19,20,21,22,23,24,25,26,27])
        AddSingleElement(res,EN.Triangle_3,[28,29,30])
        AddSingleElement(res,EN.Triangle_6,[31,32,33,34,35,36])



        res.GenerateManufacturedOriginalIDs()
        res.PrepareForOutput()
        res.props["ParafacDims"] = 2

        return res


    # 1D X 1D
    a = Create1DMesh(0)
    b = Create1DMesh(1)
    res = MeshTensorProd([a, b])
    from BasicTools.Helpers.Tests import TestTempDir
    from BasicTools.Actions.OpenInParaView import OpenInParaView
    TestTempDir.SetTempPath(r"C:\\Users\\felip\AppData\\Local\\Temp\BasicTools_Test_Directory_13wy1vrf_safe_to_delete")
    #if GUI:
        #OpenInParaView(res,filename="toto1D1D.xdmf")
    # 1D X 1D X 1D
    a = Create1DMesh(0)
    b = Create1DMesh(1)
    c = Create1DMesh(2)
    res = MeshTensorProd([a, b, c])
    #if GUI:
        #OpenInParaView(res,filename="toto1D1D1D.xdmf")

    a = Create2DMesh()
    b = Create1DMesh(0)
    print(a.props)
    print(b.props)
    res = MeshTensorProd([a, b])
    if GUI:
        OpenInParaView(res,filename="toto1D2D.xdmf")


    return "ok"
if __name__ == "__main__":
    CheckIntegrity(True) # pragma no cover