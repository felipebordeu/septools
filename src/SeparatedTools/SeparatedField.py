# -*- coding: utf-8 -*-


class SeparatedField():
    def __init__(self):
        super(SeparatedField,self).__init__()
        self.storage = []

    def CopyStructure(self):
        res = type(self)()
        res.SetNumberOfPartitions(self.GetNumberOfPartitions())
        for cpt in range(self.GetNumberOfPartitions()):
            res.SetPartition(cpt,self.GetPartition(cpt).CopyStructure())
        return res

    def View(self,modes=None):
        res = self.CopyStructure()
        res.SetNumberOfPartitions(self.GetNumberOfPartitions())
        for cpt in range(self.GetNumberOfPartitions()):
            res.SetPartition(cpt,self.GetPartition(cpt).View(modes))
        return res

    def GetNumberOfPartitions(self):
        return len(self.storage)

    def SetNumberOfPartitions(self, nbpar):
        self.storage = [None] *nbpar

    def SetPartition(self,i,par):
        self.storage[i] = par

    def GetPartition(self,i):
        return self.storage[i]

    def SetName(self,name):
        for par in self.storage:
            par.name = name

    def GetNumberOfModes(self):
        for par in self.storage:
            return par.GetNumberOfModes()


    def GetName(self):
        return self.GetPartition(0).GetName()

    def GetMeshes(self):
        res  =[]
        for ipar in range(self.GetNumberOfPartitions()):
            res.append(self.GetPartition(ipar).mesh)
        return res

    def Allocate(self,nbmodes,val=0):
        for par in self.storage:
            par.Allocate(nbmodes,val)

    def __neg__(self):
        res = self.View()
        res.storage[0] = -self.storage[0]
        return res

    def __mul__(self,other):
        import numbers
        if isinstance(other,type(self)):
            res = self.CopyStructure()
            res.Allocate(self.GetNumberOfModes()*other.GetNumberOfModes())
            for p in range(self.GetNumberOfPartitions()):
                a = self.GetPartition(p)
                b = other.GetPartition(p)
                r = res.GetPartition(p)
                r.SetDataWithBinaryOperation(a,b,'kron')
            return res
        elif isinstance(other,numbers.Number):
            if other == 1 :
                return self
            res = self.View()
            res.storage[0] *= res.storage[0]*other
            return res

        else:
            raise(NotImplementedError())

    def __rmul__(self,other):
        import numbers
        #if isinstance(other,type(self)):
        #    return self*other
        if isinstance(other,numbers.Number):
            return self*other
        else:
            raise(NotImplementedError())

    def __truediv__(self,other):
        from SeparatedTools.MiscTools import Quotient
        return Quotient(self,other)

    def __sub__(self, other):
        return self + (-other)

    def __add__(self, other):
        import numbers
        if isinstance(other,type(self)):
            res = self.CopyStructure()
            res.Allocate(self.GetNumberOfModes()+other.GetNumberOfModes())
            for p in range(self.GetNumberOfPartitions()):
                a = self.GetPartition(p)
                b = other.GetPartition(p)
                r = res.GetPartition(p)
                r.SetDataWithBinaryOperation(a,b,'concatenation')
            return res
        elif isinstance(other,numbers.Number):
            res = self.CopyStructure()
            res.Allocate(self.GetNumberOfModes()+1)
            ones = res.CopyStructure()
            ones.Allocate(1,val=1.)
            for p in range(self.GetNumberOfPartitions()):
                a = self.GetPartition(p)
                b = ones.GetPartition(p)
                if p == 0:
                    b *= other
                r = res.GetPartition(p)
                r.SetDataWithBinaryOperation(a,b,'concatenation')
            return res
        else:
            print(type(other) )
            print(other)
            raise(NotImplementedError())

    def __radd__(self,other):
        return self.__add__(other)

    def __str__(self):
        res = ""
        res += "Separated field \n"
        if self.GetNumberOfPartitions():
            res += "  Name:" + self.GetPartition(0).GetName() + "\n"
            res += "  # modes : {}\n".format(self.GetNumberOfModes())
        res += "   storage : " + str([str(self.GetPartition(x)) for x in range(self.GetNumberOfPartitions()) ]) + "\n"
        return res

    def norm(self):
        res = 0
        nm = self.GetNumberOfModes()
        for i in range(nm):
            for j in range(nm):
                lres = 1
                for k in range(self.GetNumberOfPartitions()):
                    lres *= self.GetPartition(k).GetMode(i).dot(self.GetPartition(k).GetMode(j))
                res += lres
        return res

    def Random(self):
        for ipar in range(self.GetNumberOfPartitions()):
            self.GetPartition(ipar).Random()

def CheckIntegrity():
    from SeparatedTools.Tests import CreateTestMultiModeField

    T_xy = CreateTestMultiModeField("t",2)
    print(T_xy.GetNumberOfModes())
    data = T_xy.GetMode(0)
    data = 2*data+1
    T_xy.SetMode(1,data)

    T_z = CreateTestMultiModeField("t",1,FE=False)
    print(T_z.GetNumberOfModes())


    _T_p1p2p3 = CreateTestMultiModeField("t",3)

    ###################################################
    obj = SeparatedField()
    obj.SetNumberOfPartitions(2)
    obj.SetPartition(0,T_xy)
    obj.SetPartition(1,T_z)

    obj.GetMeshes()

    print("T_z    -> " + str(T_z.GetNumberOfModes()))
    print(obj.SetName("p"))
    print(obj.GetName())
    print(obj)

    obj2 = obj.CopyStructure()
    obj2 = obj.View()

    print(obj2)
    obj2.Allocate(4,val=0)
    print("T_z    -> " + str(T_z.GetNumberOfModes()))

    _negObj = -obj

    Obj = 2*obj

    Obj = obj*obj

    Obj = obj+obj
    Obj = obj-obj


    Obj.__add__(2)
    Obj = 2+obj

    try:
        Obj = obj*"3"
        raise("This operation must fail") #pragma no cover
    except:
        pass

    try:
        Obj = "3"*obj
        raise("This operation must fail") #pragma no cover
    except:
        pass

    try:
        Obj = "3"+obj
        raise("This operation must fail") #pragma no cover
    except:
        pass

    try:
        Obj = obj+"3"
        raise("This operation must fail") #pragma no cover
    except:
        pass

    return "ok"

if __name__ == '__main__':# pragma: no cover
    print(CheckIntegrity())
