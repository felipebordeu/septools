# -*- coding: utf-8 -*-

import numpy as np

from BasicTools.FE.IntegrationsRules import IntegrationRulesAlmanac as IntegrationRulesAlmanac
from BasicTools.FE.Fields.FieldBase import FieldBase
from BasicTools.Helpers.TextFormatHelper import TFormat

class MultiModeIPField(FieldBase):
    def __init__(self,name=None,mesh=None,rule=None,ruleName=None,data=None):
        super(MultiModeIPField,self).__init__(name=name, mesh=mesh)
        if data is None:
            self.data = {}
        else:
            self.data = data
        self.rule = None
        self.SetRule(ruleName=ruleName,rule=rule)
        self.modeSize = 0
        self.Allocate(0)

    def CopyStructure(self):
        res = type(self)(name=self.name,
                               mesh=self.mesh,
                               rule=self.rule)
        res.Allocate(0)
        return res

    def View(self,modes=None):
        res = self.CopyStructure()

        if modes is None:
            res.data = self.data
        else:
            res.data = {k:d[modes,:,:] for k,d in self.data.items() }

        return res

    def SetRule(self,ruleName=None,rule=None):
        if ruleName is not None and rule is not None:
            raise(Exception("must give ruleName or rule not both"))
        if ruleName is not None:
            if not ruleName in IntegrationRulesAlmanac:
                print("Integration rule not found, possible are: ")
                print(list(IntegrationRulesAlmanac.keys()))
            self.rule = IntegrationRulesAlmanac[ruleName]
        else :
            self.rule = rule

    def GetRuleFor(self,elemtype):
        return self.rule[elemtype]

    def GetNumberOfValuesPerMode(self):
        return  self.modeSize

    def GetMode(self,i):
        res = np.empty(self.GetNumberOfValuesPerMode(),dtype=float)
        cpt=0
        for values in self.data.values():
            s = values.shape[1]*values.shape[2]
            res[cpt:cpt+s] = values[i,:,:].ravel()
            cpt +=s
        return res

    def SetMode(self,i,data):
        cpt=0
        for values in self.data.values():
            s = values.shape[1]*values.shape[2]
            d = data[cpt:cpt+s]
            values[i,:,:] = d.reshape(values.shape[1:] )
            cpt +=s

    def SetModeFor(self,elemtype,i, data):
        self.data[elemtype][i,:,:] = data

    def GetModeFor(self,elemtype,mode):
        return self.data[elemtype][mode,:,:]

    def Allocate(self,nbmodes,val=0):
        self.modeSize = 0
        self.data = {}
        for name,data in self.mesh.elements.items():
            nbItegPoints = len(self.GetRuleFor(name)[1])
            nbElements = data.GetNumberOfElements()
            self.data[name] = np.zeros((nbmodes,nbElements,nbItegPoints), dtype=np.float)
            self.data[name] += val
            self.modeSize += nbElements*nbItegPoints

    def GetNumberOfModes(self):
        for _k,d in self.data.items():
            return d.shape[0]

    def GetHeavyData(self):
        return self.data

    def CheckCompatiblility(self,B):
        if isinstance(B,type(self)):
            if id(self.mesh) != id(B.mesh):
                raise (Exception("The support of the fields are not the same"))
            if id(self.rule) != id(B.rule):
                raise (Exception("The rules of the fields are not the same"))

    def __str__(self):
        res =  TFormat.InBlue("IPField")+"\n"
        # ("+str(self.ncoomp)+")
        if self.name is not None:
            res += " name : "+ str(self.name) + "\n"
            res += str({ name:data.shape for name,data in self.data.items()} )  + "\n"
        return res

    def unaryOp(self,op):
        res = type(self)(name = None, mesh=self.mesh,rule=self.rule )
        res.data = { key:op(self.data[key]) for key in self.data.keys()}
        return res

    def binaryOp(self,other,op):
        self.CheckCompatiblility(other)
        res = type(self)(name = None, mesh=self.mesh,rule=self.rule  )
        if isinstance(other,type(self)):
            res.data = { key:op(self.data[key],other.data[key]) for key in set(self.data.keys()).union(other.data.keys())}
        else:
            res.data = { key:op(self.data[key],other) for key in self.data.keys()}

        return res

    def SetDataWithBinaryOperation(self,a,b,op):
        from SeparatedTools.MiscTools import Expand

        if op == "concatenation":
            for k in a.data:
                adata = a.data[k]
                bdata = b.data[k]
                self.data[k] = np.concatenate((adata,bdata),axis=0)
        elif op == "kron":
            for k in a.data:
                sizes = [a.GetNumberOfModes(), b.GetNumberOfModes() ]
                for cpt,activeModes in enumerate(Expand(sizes)):
                    d = a.GetModeFor(k,activeModes[0])*b.GetModeFor(k,activeModes[1])
                    self.SetModeFor(k,cpt, d )
        else:
            raise("op not available")

    def Random(self):
        def op(data):
            return np.random.random(data.shape)
        self.data  =  self.unaryOp(op).data

def CheckIntegrity():
    from BasicTools.Containers.UnstructuredMeshCreationTools import CreateCube
    mesh = CreateCube([2.,3.,4.],[-1.0,-1.0,-1.0],[2./10, 2./10,2./10])
    obj = MultiModeIPField(name = "u", mesh=mesh,ruleName="LagrangeIsoParam")
    try:
        _o = MultiModeIPField(name = "u", mesh=mesh,ruleName="NoExistentRule")
        raise("the line before must fail") # pragma: no cover
    except:
        pass

    try:
        _o = MultiModeIPField(name = "u", mesh=mesh)
        raise("the line before must fail") # pragma: no cover
    except:
        pass

    try:
        _o = MultiModeIPField(name = "u", mesh=mesh,ruleName="LagrangeIsoParam",rule=obj.rule)
        raise("the line before must fail") # pragma: no cover
    except:
        pass


    obj.Allocate(2,0)
    import BasicTools.Containers.ElementNames as EN
    _data = obj.GetModeFor(EN.Hexaedron_8,1)

    print("Number of modes of obj : " +str(obj.GetNumberOfModes()) )
    obj2 = MultiModeIPField(name = "u", mesh=mesh,rule=obj.rule,data=obj.data)

    obj.CheckCompatiblility(obj2)

    mesh2 = CreateCube([2.,3.,4.],[-1.0,-1.0,-1.0],[2./10, 2./10,2./10])
    objdiff = MultiModeIPField(name = "u", mesh=mesh2,ruleName="LagrangeIsoParam")
    try:
        objdiff.CheckCompatiblility(obj)
        raise("the line before must fail") # pragma: no cover
    except:
        pass

    objdiff = MultiModeIPField(name = "u", mesh=mesh,ruleName="LagrangeP1")
    try:
        objdiff.CheckCompatiblility(obj)
        raise("the line before must fail") # pragma: no cover
    except:
        pass


    print(obj)

    obj3 = -obj

    obj4 = obj3 + obj +1
    obj4.View(np.s_[0])

    obj5 = obj4.CopyStructure()

    try:
        obj5.SetDataWithBinaryOperation(obj3,obj4,'opNonAvailable')
        raise("This operation must fail") #pragma no cover
    except:
        pass

    return "ok"

if __name__ == '__main__':# pragma: no cover
    print(CheckIntegrity())
