# -*- coding: utf-8 -*-

import numpy as np

from BasicTools.FE.Fields.FieldBase import FieldBase
from BasicTools.Helpers.BaseOutputObject import froze_it

@froze_it
class MultiModeFEField(FieldBase):
    def __init__(self,name=None,mesh=None,space=None,numbering=None,data=None):
        super(MultiModeFEField,self).__init__(name=name,mesh = mesh)
        self.internaldata = data
        self.space = space
        self.numbering = numbering

    def CopyStructure(self):
        res = type(self)(mesh=self.mesh,
                        space=self.space,
                        numbering=self.numbering,
                        data=np.zeros((0,self.GetNumberOfValuesPerMode()) )  )

        return res
    def View(self,modes=None):
        res = self.CopyStructure()
        if modes is None:
            res.internaldata = self.internaldata
        else:
            res.internaldata = self.internaldata[modes,:]
        return res

    def Allocate(self,nbmodes,val=0):
        #print(self.internaldata)
        ndofs = self.GetNumberOfValuesPerMode()

        if val == 0:
            self.internaldata = np.zeros((nbmodes,ndofs),dtype=np.float)
        else:
            self.internaldata = np.ones((nbmodes,ndofs),dtype=np.float)*val

    def GetNumberOfModes(self):
        return self.internaldata.shape[0]

    def GetNumberOfValuesPerMode(self):
        if self.numbering is None:
            ndofs = self.internaldata.shape[1]
        else:
            ndofs = self.numbering["size"]
        return ndofs

    def SetMode(self,i, data):
        self.internaldata[i,:] = data

    def GetMode(self,i):
        return self.internaldata[i,:]

    def GetHeavyData(self):
        return self.internaldata

    def CheckCompatiblility(self,B):
        if isinstance(B,type(self)):
            if id(self.mesh) != id(B.mesh):
                raise (Exception("The support of the fields are not the same"))
            if id(self.space) != id(B.space):
                raise (Exception("The space of the fields are not the same"))
            if id(self.numbering) != id(B.numbering):
                raise (Exception("The numbering of the fields are not the same"))
            if self.GetNumberOfModes() != B.GetNumberOfModes():
                raise (Exception("The number of modes of the fields are not the same"))

    def unaryOp(self,op):
        res = self.CopyStructure()
        res.internaldata = op(self.internaldata)
        return res

    def binaryOp(self,other,op):
        self.CheckCompatiblility(other)
        res = type(self)(name = None,mesh=self.mesh,space=self.space, numbering = self.numbering )
        if isinstance(other,type(self)):
            res.internaldata = op(self.internaldata,other.internaldata)
        else:
            res.internaldata = op(self.internaldata,other)
        return res

    def SetDataWithBinaryOperation(self,a,b,op):
        from SeparatedTools.MiscTools import Expand

        if op == "concatenation":
            adata = a.internaldata
            bdata = b.internaldata
            self.internaldata = np.concatenate((adata,bdata),axis=0)
        elif op == "kron":
            sizes = [a.GetNumberOfModes(), b.GetNumberOfModes() ]
            self.Allocate(np.prod(sizes))
            for cpt,activeModes in enumerate(Expand(sizes)):
               self.SetMode(cpt,a.GetMode(activeModes[0])*b.GetMode(activeModes[1]))
        else:
            raise("op not available")

    def Random(self):
        self.internaldata = np.random.random(self.internaldata.shape)

    def __str__(self):
        res = "MultiModeFEField({})".format(self.internaldata.shape)
        return res

def CheckIntegrity(GUI=False):


    ####################    ###############################
    from SeparatedTools.Tests import CreateTestMultiModeField
    T_xy = CreateTestMultiModeField("t",2,FE=True)
    print(T_xy.GetNumberOfModes())
    data = T_xy.GetMode(0)
    data = 2*data+1
    T_xy.SetMode(1,data)

    T_p1p2p3 = CreateTestMultiModeField("t",3)

    T_p1p2p3_copy = T_p1p2p3.CopyStructure()
    T_p1p2p3_copy = T_p1p2p3.View()
    T_p1p2p3_copy.Allocate(2)

    T_p1p2p3_copy.GetHeavyData()


    T_xy_2 = CreateTestMultiModeField("t",2,FE=True)
    try:
        T_xy.CheckCompatiblility(T_xy_2)
        raise("the line before must fail") # pragma: no cover
    except:
        pass

    T3 = -T_xy
    print(T3)

    T4 = -T_xy+ 2*T_xy + 2
    print(T4)
    print(T4.View(np.s_[0]))

    T4.numbering = None
    print(T4.GetNumberOfValuesPerMode())

    T_p1p2p3_copy.Allocate(0)
    try:
        T_p1p2p3.CheckCompatiblility(T_p1p2p3_copy)
        raise(Exception("This statement must fail")) #pragma no cover
    except:
        pass

    T_p1p2p3_copy.numbering= None
    try:
        T_p1p2p3.CheckCompatiblility(T_p1p2p3_copy)
        raise(Exception("This statement must fail")) #pragma no cover
    except:
        pass

    T_p1p2p3_copy.space = None
    try:
        T_p1p2p3.CheckCompatiblility(T_p1p2p3_copy)
        raise(Exception("This statement must fail")) #pragma no cover
    except:
        pass


    try:
        T_p1p2p3.SetDataWithBinaryOperation(T_p1p2p3.copy(),T_p1p2p3.copy(),'opNonAvailable')
        raise("This operation must fail") #pragma no cover
    except:
        pass

    return  'ok'

if __name__ == '__main__':
    print(CheckIntegrity(True))# pragma: no cover
