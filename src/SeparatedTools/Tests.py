# -*- coding: utf-8 -*-


def CreateTestMultiModeField(name,dim=3,FE=True):

    if dim == 3:
        from BasicTools.Containers.UnstructuredMeshCreationTools import CreateCube
        mesh = CreateCube([2.,3.,4.],[-1.0,-1.0,-1.0],[2./10, 2./10,2./10])
    elif dim == 2:
        from BasicTools.Containers.UnstructuredMeshCreationTools import CreateSquare
        mesh = CreateSquare([5,6.],[-1.0,-1.0],[5./10, 6./10])
    else:
        from BasicTools.Containers.UnstructuredMeshCreationTools import CreateUniformMeshOfBars
        mesh = CreateUniformMeshOfBars(0,1,7)



    from BasicTools.FE.FETools import PrepareFEComputation
    spaces, numberings, _offset, _NGauss = PrepareFEComputation(mesh,numberOfComponents=1)


    if FE:
        from SeparatedTools.MultiModeFEField import MultiModeFEField
        field = MultiModeFEField(name = name,mesh=mesh,space=spaces,numbering=numberings[0])
    else:
        from SeparatedTools.MultiModeIPField import MultiModeIPField
        field = MultiModeIPField(name = name,mesh=mesh,ruleName="LagrangeIsoParam")

    field.Allocate(2,val=1)

    return field

def CreateTestSepField(nbpar=2):
    from SeparatedTools.SeparatedField import SeparatedField
    res = SeparatedField()

    res.SetNumberOfPartitions(nbpar)
    for ipar in range(nbpar):
        dim = ipar%3+1
        res.SetPartition(ipar,CreateTestMultiModeField("",dim=dim,FE=True) )
    return res

def CheckIntegrity(GUI=False):
    for dim in [1,2,3]:
        for FE in [True,False]:
            field = CreateTestMultiModeField("U",dim,FE)
            print(field)
    return "ok"

if __name__ == '__main__':
    print(CheckIntegrity(True))# pragma: no cover
