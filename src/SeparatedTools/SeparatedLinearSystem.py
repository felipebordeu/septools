# -*- coding: utf-8 -*-

import numpy as np

from BasicTools.Linalg.LinearSolver import LinearProblem

from SeparatedTools.MultiModeOperator import MultiModeOperator
from SeparatedTools.SeparatedOperator import SeparatedOp
from SeparatedTools.SeparatedField import SeparatedField

class SeparatedLinearSystem():
    def __init__(self):
        self.KK = SeparatedOp()
        self.FF = SeparatedField()
        self.LinearSolvers = []
        self.meshes = []
        self.unkownFields  = []


    def SetNumberOfPartitions(self,nbpar):
        self.KK.SetNumberOfPartitions(nbpar)
        self.FF.SetNumberOfPartitions(nbpar)
        self.LinearSolvers = [LinearProblem() for i in range(nbpar)]

    def GetNumberOfPartitions(self):
        return self.KK.GetNumberOfPartitions()

    def GetNumberOfKKModes(self):
        return self.KK.GetNumberOfModes()

    def DotK(self,sepvec):
        return self.KK.Dot(sepvec)

    def PreSolve(self):
        for i in range(self.GetNumberOfPartitions()):
            lsConstraints = self.LinearSolvers[i].constraints
            mesh = self.meshes[i]
            fieldpar = [f.GetPartition(i) for f in self.unkownFields ]
            lsConstraints.ComputeConstraintsEquations(mesh,fieldpar)

    def PushSolToUknownsFields(self, sepsol):
        for ipar in range(sepsol.GetNumberOfPartitions()):
            offset =0
            fieldspar = [f.GetPartition(ipar) for f in self.unkownFields ]
            solpar = sepsol.GetPartition(ipar)
            for f in fieldspar:
                f.internaldata = solpar.internaldata[:,offset:offset+f.numbering["size"]]
                offset += f.numbering["size"]

def AddSeparatedLinearSystems(A,B):

    res = SeparatedLinearSystem()
    res.SetNumberOfPartitions(A.GetNumberOfPartitions())

    if A.unkownFields is B.unkownFields:
        res.unkownFields = A.unkownFields
        res.meshes = A.meshes
    else:
        raise(Exception("Incompatible separated linear systems"))


    res.FF = A.FF + B.FF

    for i in range(res.GetNumberOfPartitions()):
        punkownFields = [field.GetPartition(i) for field in res.unkownFields]
        cpt = np.sum([pf.numbering["size"] for pf in punkownFields])
        res.LinearSolvers[i].constraints.SetNumberOfDofs(cpt)
        KK = MultiModeOperator()
        Apar = A.KK.GetPartition(i)
        Bpar = B.KK.GetPartition(i)
        KK.SetDataWithBinaryOperation(Apar,Bpar,"concatenation")

        res.KK.SetPartition(i,KK)

    return res


def CheckIntegrity():
    import numpy as np
    from SeparatedTools.Tests import CreateTestSepField
    field = CreateTestSepField(2)


    obj = SeparatedLinearSystem()
    obj.SetNumberOfPartitions(field.GetNumberOfPartitions())
    obj.GetNumberOfPartitions()

    from SeparatedTools.SeparatedOperator import SeparatedImplicitIdentityOperator
    objI = SeparatedImplicitIdentityOperator()
    objI.SetNumberOfPartitions(field.GetNumberOfPartitions())
    obj.KK = objI
    obj.DotK(field)

    obj.GetNumberOfKKModes()

    return "ok"

if __name__ == '__main__':# pragma: no cover
    print(CheckIntegrity())
