# -*- coding: utf-8 -*-

def Compress(sepvec):
    nbpar = sepvec.GetNumberOfPartitions()
    from SeparatedTools.SeparatedLinearSystem import SeparatedLinearSystem

    sys = SeparatedLinearSystem()
    sys.SetNumberOfPartitions(sepvec.GetNumberOfPartitions() )
    sys.FF = sepvec
    sys.meshes = [sepvec.GetPartition(ipar).mesh for ipar in range(nbpar) ]
    from SeparatedTools.SeparatedOperator import SeparatedImplicitIdentityOperator
    sys.KK = SeparatedImplicitIdentityOperator()
    sys.KK.SetSizeFromSepvec(sepvec)

    from SeparatedTools.PGDSolver import PGDSolver
    solver = PGDSolver()
#    solver.verbose =True
    for ls in sys.LinearSolvers:
        ls.SetAlgo('Direct')

    solver.PreSolve(sys)
    sol = solver.Solve(sys)
    sol.SetName(sepvec.GetName())
    return sol

def Quotient(sepvecI,sepvecII):
    #print("quotient of {}/{}".format(sepvecI.GetName(),sepvecII.GetName() ))

    nbpar = sepvecII.GetNumberOfPartitions()
    from SeparatedTools.SeparatedLinearSystem import SeparatedLinearSystem

    sys = SeparatedLinearSystem()
    sys.SetNumberOfPartitions(sepvecII.GetNumberOfPartitions() )
    if sepvecI == 1:
        ones = sepvecII.CopyStructure()
        ones.Allocate(1,val=1)
        sys.FF = ones
    else:
        sys.FF = sepvecI
    sys.meshes = [sepvecII.GetPartition(ipar).mesh for ipar in range(nbpar) ]
    from SeparatedTools.SeparatedOperator import SeparatedDiagonalOpFromVecOperator

    sys.KK = SeparatedDiagonalOpFromVecOperator(sepvecII)

    from SeparatedTools.PGDSolver import PGDSolver
    solver = PGDSolver()
#    solver.verbose =True
    solver.PreSolve(sys)
    sol = solver.Solve(sys)
    sol.SetName("("+sys.FF.GetName()+"/"+sepvecII.GetName()+")")
    return sol

def GetAlphas(sepvec):
    import numpy as np
    nbmodes = sepvec.GetNumberOfModes()
    res = np.ones(nbmodes)
    for i in range(nbmodes):
        for ipar in range(sepvec.GetNumberOfPartitions()):
            res[i] *= np.linalg.norm(sepvec.GetPartition(ipar).GetMode(i))
    return res
# Export

def ComputeAuxiliaryFields(fields,ops,constants=None,elementFilter=None):
    from BasicTools.FE.SymWeakForm import GetField,space
    from sympy.core.function import AppliedUndef


    if constants is None:
        constants = {}

    res = []
    fieldextractor = []

    from sympy.abc import x
    from sympy.utilities.lambdify import lambdify, implemented_function
    from sympy import Function

    for f in fields:
        print(f.GetName())
        symf = implemented_function(f.GetName() , lambda : f)
        sf = GetField(f.GetName(),size=1,sdim=2)[0]
        fieldextractor.append( (sf,symf(),f ) )

    from sympy import Symbol
    for k,v in constants.items():
        fieldextractor.append((Symbol(k),v,v))

    names = [x[0] for x in fieldextractor]
    datas = [x[2] for x in fieldextractor]

    for name in ops:
        symop = ops[name][0]
        lam_f = lambdify(names,symop)
        #import inspect
        #print(inspect.getsource(lam_f))
        resField = lam_f(*datas)
        resField.SetName(name)
        res.append(resField)

    return res

def SetSeparationNamesIntoMeshes(meshes,separation):
    for m,coords in zip(meshes,separation):
        m.props["ParafacDims"] = len(coords)
        for cpt,name in enumerate(coords):
            m.props["ParafacDim{}".format(cpt)] = str(name)

class Expand():
    def __init__(self,sizes):
        self.sizes = sizes[:]

    def __iter__(self):

        lastNotNoneIndex = -1
        for i in range(len(self.sizes)-1,-1,-1):
            if self.sizes[i] is None:
                continue
            lastNotNoneIndex = i
            break

        cpt = [0 if x is not None else None for x in self.sizes]

        if lastNotNoneIndex == -1:
            yield cpt
            return

        while(cpt[lastNotNoneIndex] < self.sizes[lastNotNoneIndex] ):
            yield cpt

            for i in range(len(self.sizes)):
                if self.sizes[i] == None:
                    continue
                cpt[i] +=1
                break

            for i in range(lastNotNoneIndex):
                if cpt[i] is None :
                    continue

                if cpt[i] == self.sizes[i]:
                    cpt[i] = 0

                    for i in range(i+1,lastNotNoneIndex+1):
                        if self.sizes[i] == None:
                            continue
                        cpt[i] +=1
                        break

def PreparePGDComputation(name, meshes,numberOfComponents=1,integrationPointField=False):

    from SeparatedTools.SeparatedField import SeparatedField
    from SeparatedTools.MultiModeFEField import MultiModeFEField
    from SeparatedTools.MultiModeIPField import MultiModeIPField

    res = [None]*numberOfComponents
    for i in range(numberOfComponents):
        res[i] = SeparatedField()
        res[i].SetNumberOfPartitions(len(meshes))

    from BasicTools.FE.FETools import PrepareFEComputation

    for cpt,mesh in enumerate(meshes):
        spaces_, numberings_, _offset, _NGauss = PrepareFEComputation(mesh,numberOfComponents=1)
        for i in range(numberOfComponents):
            realname = name if numberOfComponents == 1 else name+"_"+str(i)
            if integrationPointField:
                field = MultiModeIPField(name = realname ,mesh=mesh,ruleName="LagrangeIsoParam")
            else:
                field = MultiModeFEField(name = realname ,mesh=mesh,space=spaces_,numbering=numberings_[0])
            res[i].SetPartition(cpt,field)

    return res


def CheckIntegrity(GUI=False):

    datas = [[2,3,4],
             [None,2,3,4],
             [2,None,3,4],
             [2,3,None,4],
             [2,3,4,None],
             [2,None,None,4],
             [None,None]
             ]
    for data in datas:
        for cpt in Expand(data):
           print(cpt)

    from BasicTools.Containers.UnstructuredMeshCreationTools import CreateCube
    mesh = CreateCube()
    PreparePGDComputation("u",[mesh],1)
    PreparePGDComputation("u",[mesh],3)

    from BasicTools.Containers.UnstructuredMeshCreationTools import CreateUniformMeshOfBars
    mesh1 = CreateUniformMeshOfBars(0,1,10)
    mesh2 = CreateUniformMeshOfBars(0,1,11)

    Meshes = [mesh1, mesh2]

    from SeparatedTools .MiscTools import SetSeparationNamesIntoMeshes
    from BasicTools.FE.SymWeakForm import GetField,space
    from sympy import Symbol

    separation = [space[0:1],space[1:2]]

    SetSeparationNamesIntoMeshes(Meshes,separation)

    T = PreparePGDComputation("T",Meshes,integrationPointField=True)[0]
    T.Allocate(1,val=1)


    T = PreparePGDComputation("T",Meshes)[0]
    T.Allocate(1,val=1)

    X = T.CopyStructure()
    X.SetName("X")
    X.Allocate(1,val=1)
    X.GetPartition(0).SetMode(0,mesh1.nodes[:,0])

    Y = T.CopyStructure()
    Y.SetName("Y")
    Y.Allocate(1,val=1)
    Y.GetPartition(1).SetMode(0,mesh2.nodes[:,0])

    # tryin to compute (X*Y+1)/(X+Y+T+1)

    symX = GetField("X",size=1,sdim=2)[0]
    symY = GetField("Y",size=1,sdim=2)[0]
    symT = GetField("T",size=1,sdim=2)[0]
    symB = Symbol("B")

    eq = (symX*symY)/(1+symX+symY+symT+1)

    fields = [T,X,Y]
    constants = {"B":1.}

    ops = {"auxfield":(eq,None)}


    res = ComputeAuxiliaryFields(fields,ops)

    ops = {"auxfield":(eq*symB,None)}
    res = ComputeAuxiliaryFields(fields,ops,constants)

    print(res[0])

    res = Compress(X*Y+1)

    print(GetAlphas(res))

    print(Quotient(1,res))
    return "ok"

if __name__ == '__main__':
    print(CheckIntegrity(True))# pragma: no cover