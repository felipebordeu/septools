# -*- coding: utf-8 -*-

import numpy as np

from BasicTools.FE.Fields.FieldBase import FieldBase
from BasicTools.Helpers.BaseOutputObject import froze_it,BaseOutputObject
from SeparatedTools.SeparatedField import SeparatedField

@froze_it
class MultiModeOperator(BaseOutputObject):
    def __init__(self):
        super(MultiModeOperator,self).__init__()
        self.data = []

    def GetNumberOfModes(self):
        return len(self.data)

    def SetNumberOfModes(self,nbops):
        self.data = [None]*nbops

    def GetName(self):
        return ""

    def GetOp(self,i):
        return self.data[i]

    def SetDataWithBinaryOperation(self,a,b,op):
        from SeparatedTools.MiscTools import Expand

        if op == "concatenation":
            adata = a.data
            bdata = b.data
            self.data.extend(adata)
            self.data.extend(bdata)
        else:
            raise RuntimeError("op not available")

@froze_it
class MultiModeOperatorTOperator(BaseOutputObject):
    def __init__(self,op):
        #super(MultiModeOperatorTOperator,self).__init__()
        #self.name =""
        #self.leftMesh = None
        #self.rightMesh = None
        #self.leftNumbering = []
        #self.rightNmbering = []
        self.mmop = op

    def GetNumberOfModes(self):
        return self.mmop.GetNumberOfModes()**2

    def SetNumberOfModes(self,nbops):
        raise NotImplementedError()

    def GetName(self):
        return ""

    def GetOp(self,mn):
        i = mn//self.mmop.GetNumberOfModes()
        j = mn-(self.mmop.GetNumberOfModes()*i)
        return self.mmop.GetOp(i).transpose().dot(self.mmop.GetOp(j))




#class SeparatedImplicitDot_Op_vec(SeparatedField):
#    def __init__(self):
#        self.Op
#        self.vec
#
#    def Dot(self.)


def CheckIntegrity(GUI=False):
    obj = MultiModeOperator()
    obj.GetNumberOfModes()
    obj.SetNumberOfModes(2)
    obj.data[0] = np.eye(5)
    obj.data[1] = np.eye(5) +1

    obj.GetOp(0)

    #####################"  MultiModeOperatorTOperator #################
    obj2 = MultiModeOperatorTOperator(obj)
    assert obj2.GetNumberOfModes() == 4

    try:
        obj2.SetNumberOfModes(0)
        raise RuntimeError() #pragma no cover
    except:
        pass

    obj2.GetName()
    print(obj2.GetOp(0))

    return "ok"

if __name__ == '__main__':
    print(CheckIntegrity(True))# pragma no cover