# -*- coding: utf-8 -*-

from itertools import product
import numpy as np
from BasicTools.Helpers.TextFormatHelper import TFormat as TF

class PGDSolver():

    def __init__(self):
        super(PGDSolver,self).__init__()
        self.maxCalculatedTerms = 100
        self.maxNumberOfFixPointIterations = 100
        self.lookBackCounter = 10
        self.updateU = True
        self.nbpar = 0
        self.diffAlphaStopIter =  1e-8
        self.alphaAlpha0 = 1e-8
        self.verbose = False

    def UpdateNormMatrix(self,sepvecA,sepvecB,out,ipar):
        nbModesA =sepvecA.GetNumberOfModes()
        nbModesB =sepvecB.GetNumberOfModes()
        parA = sepvecA.GetPartition(ipar)
        parB = sepvecB.GetPartition(ipar)
        for Ai,Bj in product(range(nbModesA), range(nbModesB)):
            out[ipar,Ai,Bj] = parA.GetMode(Ai).dot(parB.GetMode(Bj))

    def ComputeNormMatrix(self,sepvecA,sepvecB,out=None):
        nbModesA =sepvecA.GetNumberOfModes()
        nbModesB =sepvecB.GetNumberOfModes()

        if out is None:
            res = np.empty( (self.nbpar,nbModesA,nbModesB) )
        else:
            res = out

        for ipar in range(self.nbpar):
            self.UpdateNormMatrix(sepvecA,sepvecB,res,ipar)
        return res

    def UpdateNormMatrixKK(self,KK,rs,out,ipar):
        nbModesKK = KK.GetNumberOfModes()
        parKK = KK.GetPartition(ipar)
        parRS = rs.GetPartition(ipar)
        RSMode = parRS.GetMode(0)
        for KKi in range(nbModesKK):
            #print("rsmode ")
            #print(RSMode)
            #print("op : ")
            #print(parKK.GetOp(KKi))
            out[ipar,KKi,0] = parKK.GetOp(KKi).dot(RSMode).dot(RSMode)

    def ComputeNormMatrixKK(self,KK,rs,out):
        #print(" self.nbpar ", self.nbpar)
        #print(out)
        for ipar in range(self.nbpar):
            self.UpdateNormMatrixKK(KK,rs,out,ipar)
        return out

    def Contraction(self,field,norms,par):
        res = np.zeros(field.GetPartition(par).GetNumberOfValuesPerMode())
        ##.shape[1])
        for m in range(field.GetNumberOfModes()):
            factor = 1.
            for ipar in range(self.nbpar):
                if ipar == par:
                    continue
                factor *= norms[ipar,m,0]
            res += factor*field.GetPartition(par).GetMode(m)
        return res

    def ContractionKK(self,KK,norms,par):
        from scipy.sparse import coo_matrix
        res = coo_matrix(KK.GetPartition(par).GetOp(0).shape)
        for m in range(KK.GetNumberOfModes()):
            factor = 1.
            for ipar in range(self.nbpar):
                if ipar == par:
                    continue
                factor *= norms[ipar,m,0]
            res += factor*KK.GetPartition(par).GetOp(m)
        return res

    def PreSolve(self,sepSys):
        sepSys.PreSolve()

    def __UpdateTerm(self,cpt,rs,KK,FF,LinearSolvers,norm_FF_RS,OPU0,norm_OPU0_RS,OPU,norm_OPU_RS,norm_OP_RS,uStorage,OPU_Storage):

        oldAlpha = 0

        self.ComputeNormMatrix(FF,rs,out=norm_FF_RS)
        self.ComputeNormMatrix(OPU0,rs,out=norm_OPU0_RS)
        self.ComputeNormMatrix(OPU,rs,out=norm_OPU_RS)
        #print(" norm_OP_RS ", norm_OP_RS)
        self.ComputeNormMatrixKK(KK,rs,out=norm_OP_RS)
        #print("  ", norm_OP_RS)

        for it in range(self.maxNumberOfFixPointIterations):
            for p in range(self.nbpar):
                parLinearSolver = LinearSolvers[p]

                f_rhs = self.Contraction(FF,norm_FF_RS,p)
                opu0_rhs = self.Contraction(OPU0,norm_OPU0_RS,p)
                opu_rhs = self.Contraction(OPU,norm_OPU_RS,p)

                rhs = f_rhs - opu0_rhs - opu_rhs

                lhs = self.ContractionKK(KK,norm_OP_RS,p)
                #print("KK",KK)
                #print("norm_OP_RS",norm_OP_RS)
                #print("p",p)

                #print("lhs",lhs)
                #print("lhs",lhs.shape)

                parLinearSolver.SetOp(lhs)
                parLinearSolver.u = rs.GetPartition(p).GetMode(0)
                sol = parLinearSolver.Solve(rhs)

                norm_sol = np.linalg.norm(sol)
                if p != (self.nbpar-1):
                    sol /= norm_sol
                else :
                    lastAlpha = norm_sol

                rs.GetPartition(p).SetMode(0,sol)

                self.UpdateNormMatrix(FF,rs,norm_FF_RS,p )
                self.UpdateNormMatrix(OPU0,rs,norm_OPU0_RS,p)
                self.UpdateNormMatrix(OPU,rs,norm_OPU_RS,p)
                self.UpdateNormMatrixKK(KK,rs,norm_OP_RS,p)

            if abs((lastAlpha - oldAlpha)/lastAlpha) < self.diffAlphaStopIter :
                break
            if (self.alpha0 is not None) and lastAlpha < self.alpha0* (self.alphaAlpha0 /100):
                break
            oldAlpha = lastAlpha

        op_rs = KK.Dot(rs)

        for par in range(self.nbpar):
            uStorage.GetPartition(par).SetMode(cpt,rs.GetPartition(par).GetMode(0))
            op_rs_par = op_rs.GetPartition(par)
            opupar = OPU_Storage.GetPartition(par)
            #op_rs_par = op_rs.GetPartition(par).GetHeavyData()
            #OPU_Storage.GetPartition(par).internaldata[KK.GetNumberOfModes()*cpt:KK.GetNumberOfModes()*cpt+op_rs.GetNumberOfModes(),:] = op_rs_par
            cpt2 = KK.GetNumberOfModes()*cpt
            for i in range(op_rs.GetNumberOfModes()):
                mode = op_rs_par.GetMode(i)
                opupar.SetMode(cpt2,mode)
                cpt2 += 1

        return lastAlpha,it

    def Solve(self,sepSys,u0=None,ResMin=False):
        self.alpha0 = None
        if self.verbose :
            print("PGD Solver:")
            print("  System has {} terms in the operator".format(sepSys.KK.GetNumberOfModes() ))
            print("  System has {} terms in the rhs".format(sepSys.FF.GetNumberOfModes() ))
            if u0 is not None:
                print("  u0  has {} terms ".format(u0.GetNumberOfModes() ))

            if ResMin :
                print("  Using residual minimization Alg ")

        self.nbpar = sepSys.GetNumberOfPartitions()

        if ResMin:
            from SeparatedTools.SeparatedOperator import SeparatedImplicit_OpT_Op
            KK = SeparatedImplicit_OpT_Op()
            KK.op = sepSys.KK
            KKtFF = sepSys.FF.CopyStructure()
            KKtFF.Allocate(sepSys.FF.GetNumberOfModes()*sepSys.KK.GetNumberOfModes())
            for ipar in range(self.nbpar):
                ffpar = sepSys.FF.GetPartition(ipar)
                kkpar = sepSys.KK.GetPartition(ipar)
                KKtFFpar = KKtFF.GetPartition(ipar)
                cpt = 0
                for KKj in range(kkpar.GetNumberOfModes()):
                    for m in range(ffpar.GetNumberOfModes()):
                        KKtFFpar.SetMode(cpt,kkpar.GetOp(KKj).transpose().dot(ffpar.GetMode(m)))
                        cpt +=1
            FF = KKtFF
        else:
            KK = sepSys.KK
            FF = sepSys.FF

        if u0 is None:
            u0 = FF.CopyStructure()

        if FF.GetNumberOfModes() == 0 and u0.GetNumberOfModes() == 0:
            res = FF.CopyStructure()
            res.Allocate(0)
            return res
        elif FF.norm() == 0 and u0.norm() ==0 :
            res = FF.CopyStructure()
            res.Allocate(0)
            return res

        rs = FF.CopyStructure()
        rs.Allocate(1,val=0)

        uStorage = FF.CopyStructure()
        uStorage.Allocate(self.maxCalculatedTerms,val=0)

        OPU0 = KK.Dot(u0)

        norm_FF_RS = np.empty((self.nbpar,FF.GetNumberOfModes(),1)  )
        norm_OPU0_RS = np.empty((self.nbpar,OPU0.GetNumberOfModes(),1)  )
        norm_OP_RS = np.empty((self.nbpar,KK.GetNumberOfModes(),1)  )

        norm_OPU_RS = np.empty((self.nbpar,KK.GetNumberOfModes()*self.maxCalculatedTerms,1)  )

        OPU_Storage = FF.CopyStructure()
        OPU_Storage.Allocate(KK.GetNumberOfModes()*self.maxCalculatedTerms)

        OPU = OPU_Storage.View(np.s_[0:0])


        for cpt in range(self.maxCalculatedTerms):

            u = uStorage.View(np.s_[0:cpt])
            OPU = OPU_Storage.View(np.s_[0:KK.GetNumberOfModes()*cpt])

            if self.verbose :
                print("cpt {} : ".format(cpt),end="")
                print(" "*3*(max(0,self.lookBackCounter-cpt)) , end='')
            for cpt2 in range(max(0,cpt-self.lookBackCounter),cpt):
                for ipar in range(self.nbpar):
                    rs.GetPartition(ipar).SetMode(0,u.GetPartition(ipar).GetMode(cpt2))
                    # delete ponderations in the matrix
                    #OPU_Storage.GetPartition(ipar).internaldata[KK.GetNumberOfModes()*cpt2:KK.GetNumberOfModes()*cpt2+KK.GetNumberOfModes(),:] = 0
                    zero = OPU_Storage.GetPartition(ipar).GetMode(0)*0
                    for i in range(KK.GetNumberOfModes()*cpt2,KK.GetNumberOfModes()*cpt2+KK.GetNumberOfModes()):
                        OPU_Storage.GetPartition(ipar).SetMode(i,zero)

                lastAlpha, it  = self.__UpdateTerm(cpt2,rs,
                              KK,FF,sepSys.LinearSolvers,
                              norm_FF_RS=norm_FF_RS,
                              OPU0=OPU0,
                              norm_OPU0_RS=norm_OPU0_RS,
                              OPU=OPU,
                              norm_OPU_RS=norm_OPU_RS,
                              norm_OP_RS=norm_OP_RS,
                              uStorage=uStorage,
                              OPU_Storage=OPU_Storage)
                if self.verbose:
                    print(TF.Center(str(it),width=3), end='')


            rs.Random()

            lastAlpha, it  = self.__UpdateTerm(cpt,rs,
                              KK,FF,sepSys.LinearSolvers,
                              norm_FF_RS=norm_FF_RS,
                              OPU0=OPU0,
                              norm_OPU0_RS=norm_OPU0_RS,
                              OPU=OPU,
                              norm_OPU_RS=norm_OPU_RS,
                              norm_OP_RS=norm_OP_RS,
                              uStorage=uStorage,
                              OPU_Storage=OPU_Storage)


            if self.verbose:
                print(" . {} Alpha {:.2e} ".format(it, lastAlpha))
            if cpt == 0:
                self.alpha0 = lastAlpha
            else:
                if lastAlpha < self.alpha0 *self.alphaAlpha0:
                    break

        u = uStorage.View(np.s_[0:cpt+1])

        if self.updateU:
            return u + u0
        else:
            return u


def CheckIntegrity(GUI=False):

    from SeparatedTools.SeparatedWeakForm import GetSeparatedField
    from BasicTools.FE.SymPhysics import ThermalPhysics
    from BasicTools.FE.SymWeakForm import GetField,space
    from sympy import Symbol

    from SeparatedTools.SeparatedWeakForm import SymWeakToNumWeakSeparated

    ########################## Symbolic part ########################################

    # two firenect way of construction the weak formulation
    # 1) by creatin every symbolic field with the function GetSeparateField
    separation = [space[0:1],space[1:2]]

    thermalPhysics = ThermalPhysics()
    thermalPhysics.spaceDimension = 2
    thermalPhysics.primalUnknown = GetSeparatedField(name = thermalPhysics.thermalPrimalName[0],
                                                        size = thermalPhysics.thermalPrimalName[1],
                                                        star = False ,
                                                        separation = separation)

    thermalPhysics.primalTest = GetSeparatedField(name = thermalPhysics.thermalPrimalName[0],
                                                        size = thermalPhysics.thermalPrimalName[1],
                                                        star = True ,
                                                        separation = separation)

    volumeSource =  thermalPhysics.primalTest*1
    weakForm = thermalPhysics.GetBulkFormulation() + volumeSource
    numericalFormulation = SymWeakToNumWeakSeparated(weakForm,separation=separation)

    print("--- Weak formulation -----")
    for ipar, form in enumerate(numericalFormulation):
        print("Partition {}".format(ipar))
        print(form)

    #2) by the classic way of BasicTools then the inegrator will do a substitution with
    # separated representation field, symbols are not converted (constants)
    #thermalPhysics = ThermalPhysics()
    #thermalPhysics.spaceDimension = 2
    #volumeSource =  thermalPhysics.primalTest*1
    #weakForm = thermalPhysics.GetBulkFormulation() + volumeSource

    # this part is done during the integration
    #   #from SeparatedTools.SeparatedWeakForm import SubsSeparatedRepresentation
    #   #weakForm = SubsSeparatedRepresentation(weakForm,separation=separation)
    #   #numericalFormulation = SymWeakToNumWeakSeparated(weakForm,separation=separation)

    #print("--- Weak formulation -----")
    #for ipar, form in enumerate(numericalFormulation):
    #    print("Partition {}".format(ipar))
    #    print(form)




    ################# Numerical part  ##########################
    #definition of the computation domain
    Lx = 2
    Ly = 1

    #epsion = 1e-8
    #epsilon_tilde =0
    #Max_terms = 10
    #Max_fp_iter = 20;

    Nx = 41
    Ny = 41

    from BasicTools.Containers.UnstructuredMeshCreationTools import CreateUniformMeshOfBars

    Mesh_x = CreateUniformMeshOfBars(0,Lx,Nx)
    Mesh_y = CreateUniformMeshOfBars(0,Ly,Ny)

    Meshes = [Mesh_x, Mesh_y]

    from SeparatedTools .MiscTools import SetSeparationNamesIntoMeshes
    SetSeparationNamesIntoMeshes(Meshes,separation)


    _x = Mesh_x.nodes[:,0]
    _y = Mesh_y.nodes[:,0]

    # field generations
    from SeparatedTools.MiscTools import PreparePGDComputation
    # basic FE using all the nodes and isoparametric

    T0 = PreparePGDComputation("t0",Meshes)[0]
    T0.Allocate(0,val=0)
    #T0.Allocate(1,val=0)
    # Because the numbering is computed using connectivity we can do
    #A priori terms imposing the Dirichlet BC (Eq. (2.58))
    #GX = (Lx-x)/Lx;
    #GY = y*(Ly-y);
    #T0.GetPartition(0).SetMode(0,GX)
    #T0.GetPartition(1).SetMode(0,GY)

    FF = T0.CopyStructure()
    FF.SetName("FF")
    FF.Allocate(0)
    #Separated representation of the source term (Eq. (2.59))
    #FF.Allocate(1)
    #FX = -5*np.exp(-10*(x-Lx/2)**2);
    #FY = np.exp(-10*(y-Ly/2)**2);
    #FF.GetPartition(0).SetMode(0,FX)
    #FF.GetPartition(1).SetMode(0,FY)

        #Neumann BC on upper boundary (Eq. (2.57), last line)
    #    q = -1;
        #TODO

    # Unknown
    t = T0.CopyStructure()
    t.SetName("t")
    t.Allocate(0)

    #### Definition of the integration domain ########################
    from BasicTools.Containers.Filters import ElementFilter
    from SeparatedTools.SeparatedIntegrator import SeparatedIntegrateGeneral

    elementFilter =[ElementFilter(mesh=mesh,dimensionality=1) for mesh in Meshes]

    separatedSystem = SeparatedIntegrateGeneral( meshes=Meshes,
                                                wform = numericalFormulation,
                                                unkownFields=[t],
                                                elementFilter=elementFilter)

    #dirichelet conditions

    for ipar in range(2):
        ls = separatedSystem.LinearSolvers[ipar]
        from BasicTools.FE.KR.KRBlock import KRBlock

        dirichlet = KRBlock()
        dirichlet.AddArg("t").On('L')
        dirichlet.value = lambda pos : 0
        ls.constraints.AddConstraint(dirichlet)

        dirichlet = KRBlock()
        dirichlet.AddArg("t").On('H')
        dirichlet.value = lambda pos : 0
        ls.constraints.AddConstraint(dirichlet)
        #ls.SetAlgo("CG")
        #ls.tol = 1e-4
        ls.SetAlgo("Direct")

    # Solve
    from SeparatedTools.PGDSolver import PGDSolver
    u0 = separatedSystem.FF.View()
    u0.Allocate(1,val=1)
    solver = PGDSolver()
    solver.verbose = True
    solver.updateU = False
    solver.maxNumberOfFixPointIterations = 5
    solver.PreSolve(separatedSystem)
    sol = solver.Solve(separatedSystem,u0=u0,ResMin=True)
    separatedSystem.PushSolToUknownsFields(sol)

    from SeparatedTools.MiscTools import Compress, GetAlphas
    t_c  = Compress(t)
    t_c.SetName("t_compact")
    #alphas
    alphat = GetAlphas(t)
    alphat_c = GetAlphas(t_c)

    import numpy as np
    print("Alphas t")
    print(np.array2string(alphat, formatter={'float_kind':lambda x: "%.2e" % x}))
    print(np.array2string(alphat_c, formatter={'float_kind':lambda x: "%.2e" % x}))


    from SeparatedTools.IOTools import WritePxdmf
    WritePxdmf("ThermalSolution_X_Y_2.pxdmf",Meshes,[t, t_c])
    return 'ok'

if __name__ == "__main__":
    CheckIntegrity(True) # pragma no cover