# -*- coding: utf-8 -*-

from SeparatedTools.SeparatedWeakForm import GetSeparatedField
from BasicTools.FE.SymPhysics import ThermalPhysics
from BasicTools.FE.SymWeakForm import GetField,space
from sympy import Symbol

########################## Symbolic part ########################################

separation = [space[0:1],space[1:2]]

thermalPhysics = ThermalPhysics()
thermalPhysics.spaceDimension = 2

volumeSource =  thermalPhysics.primalTest*1
weakForm = thermalPhysics.GetBulkFormulation() + volumeSource

################# Numerical part  ##########################
from BasicTools.Containers.UnstructuredMeshCreationTools import CreateUniformMeshOfBars
from SeparatedTools.MiscTools import PreparePGDComputation
from SeparatedTools .MiscTools import SetSeparationNamesIntoMeshes

#definition of the computation domain
Lx = 2
Ly = 1

Nx = 410
Ny = 411


Mesh_x = CreateUniformMeshOfBars(0,Lx,Nx)
Mesh_y = CreateUniformMeshOfBars(0,Ly,Ny)

Meshes = [Mesh_x, Mesh_y]

SetSeparationNamesIntoMeshes(Meshes,separation)

x = Mesh_x.nodes[:,0]
y = Mesh_y.nodes[:,0]

# field generations
# basic FE using all the nodes and isoparametric

T0 = PreparePGDComputation("t0",Meshes)[0]
T0.Allocate(0,val=0)
#T0.Allocate(1,val=0)
# Because the numbering is computed using connectivity we can do
#A priori terms imposing the Dirichlet BC (Eq. (2.58))
#GX = (Lx-x)/Lx;
#GY = y*(Ly-y);
#T0.GetPartition(0).SetMode(GX,0)
#T0.GetPartition(1).SetMode(GY,0)

FF = T0.CopyStructure()
FF.SetName("FF")
FF.Allocate(0)
#Separated representation of the source term (Eq. (2.59))
#FF.Allocate(1)
#FX = -5*np.exp(-10*(x-Lx/2)**2);
#FY = np.exp(-10*(y-Ly/2)**2);
#FF.GetPartition(0).SetMode(FX,0)
#FF.GetPartition(1).SetMode(FY,0)

    #Neumann BC on upper boundary (Eq. (2.57), last line)
#    q = -1;
    #TODO

# Unknown
t = T0.CopyStructure()
t.SetName("t")
t.Allocate(0)

#### Definition of the integration domain ########################
from BasicTools.Containers.Filters import ElementFilter
from SeparatedTools.SeparatedIntegrator import SeparatedIntegrateGeneral

elementFilter =[ElementFilter(mesh=mesh,dimensionality=1) for mesh in Meshes]

separatedSystem = SeparatedIntegrateGeneral( meshes=Meshes,
                                             wform = weakForm,
                                             unkownFields=[t],
                                             elementFilter=elementFilter,
                                             separation=separation)

#dirichelet conditions

for ipar in range(2):
    ls = separatedSystem.LinearSolvers[ipar]
    from BasicTools.FE.KR.KRBlock import KRBlock

    dirichlet = KRBlock()
    dirichlet.AddArg("t").On('L')
    dirichlet.value = lambda pos : 0
    ls.constraints.AddConstraint(dirichlet)

    dirichlet = KRBlock()
    dirichlet.AddArg("t").On('H')
    dirichlet.value = lambda pos : 0
    ls.constraints.AddConstraint(dirichlet)
    #ls.SetAlgo("CG")
    #ls.tol = 1e-4
    ls.SetAlgo("Direct")

# Solve
from SeparatedTools.PGDSolver import PGDSolver
solver = PGDSolver()
solver.maxNumberOfFixPointIterations = 5
solver.PreSolve(separatedSystem)
sol = solver.Solve(separatedSystem,ResMin=False)
separatedSystem.PushSolToUknownsFields(sol)

from SeparatedTools.MiscTools import Compress, GetAlphas
t_c  = Compress(t)
t_c.SetName("t_compact")
#alphas
alphat = GetAlphas(t)
alphat_c = GetAlphas(t_c)

import numpy as np
print("Alphas t")
print(np.array2string(alphat, formatter={'float_kind':lambda x: "%.2e" % x}))
print(np.array2string(alphat_c, formatter={'float_kind':lambda x: "%.2e" % x}))


from SeparatedTools.IOTools import WritePxdmf
WritePxdmf("ThermalSolution_X_Y_2.pxdmf",Meshes,[t, t_c])
