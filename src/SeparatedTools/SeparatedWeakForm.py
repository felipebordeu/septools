# -*- coding: utf-8 -*-
import numpy as np

from sympy import Symbol,Function,trace
from sympy import ones
from sympy.core.mul import Mul
from sympy.core.add import Add
from sympy.core.power import Pow
from sympy import S
from sympy.core.containers import Tuple
from sympy.core.function import AppliedUndef
from sympy.core.numbers import One

from BasicTools.FE.SymWeakForm import GetField,space
from BasicTools.FE.WeakForms.NumericalWeakForm import PyWeakMonom, PyWeakTerm, PyWeakForm
from BasicTools.FE.SymWeakForm import GetNormal

def GetSeparatedField(name,size,star=False,separation=None):
    res = ones(size,1 )
    for s in separation :
        f = GetField(name,size,star=star,sdim=0,extraCoordinates=s)
        res = res.multiply_elementwise(f)
    return res

def SubsSeparatedRepresentation(wf,separation,symbols=False):
    res = wf

    varList = wf.atoms(AppliedUndef)

    for var in varList:

        if isinstance(var,Function):
            N = GetNormal(3)
            if np.any([var == nc for nc in N]):
                continue


        varname = str(var.func)
        t = GetSeparatedField(varname,1,star=False,separation=separation)[0]
        res = res.subs(var,t)
    return res.doit()

def SeparateSymExpr(exp,separation):
    l = len(separation)

    res = [[] for i in range(l)]

    try:
        if exp.shape[0] == 1 and exp.shape[1] == 1:
            exp = exp[0,0]
    except:
        pass

    def Classified(arg):
        for cpt,s in enumerate(separation):
            for coord in s:
                if arg.diff(coord) != 0:
                    res[cpt][-1] = res[cpt][-1]*arg
                    break
            else:
                continue
            break # break the for cpt,s.. if the inner loop breaks
        else:
            res[-1][-1] = res[-1][-1]*arg
        return

    def SeparateTerm(term):
        for i in range(l):
            res[i].append(S.One)
        if term.is_Number:
            Classified(term)
        elif term.func == Mul:
            for arg in term.args:
                Classified(arg)
        else:
            Classified(term)
        return res

    if exp.is_Number:
        for i in range(l):
            res[i].append(S.One)
        Classified(exp)
    elif exp.func == Add:
        for term in exp.args:
            SeparateTerm(term)
    elif exp.func == Mul:
        SeparateTerm(exp)
    else:
        for i in range(l):
            res[i].append(S.One)
        Classified(exp)
    return res

def FlattenSeparation(separation):
    return  tuple([element for tupl in separation for element in tupl])


def CompactSymWeak(exp,unkownFields,separation):

    try:
        if exp.shape[0] == 1 and exp.shape[1] == 1:
            exp = exp[0,0]
    except:
        pass


    separation
    allcoords = FlattenSeparation(separation)

    tempData = ["temp_",0]

    ops = dict()

    def GetNewName():
        name = tempData[0] + str(tempData[1])
        tempData[1] += 1
        return name

    def CompactTerm(term):

        if term.is_Number:
            return term
        #print("term")
        #print(term)
        if term.func == Mul:
            #print("in mul {}".format(term))
            res = 1

            op = 1
            for arg in term.args:

                if np.any([ arg.has(x) for x in unkownFields ]):
                    res = res * arg
                    continue

                if isinstance(arg,Function):
                    N = GetNormal(3)
                    if np.any([arg == nc for nc in N]):
                        res = res *arg
                        continue

                op = op * arg


            if op.is_number:
                res = res * op
            elif np.any([ x[0]-op ==0  for x in ops.values() ] ):
                for _k,v in ops.items():
                    if v[0] - op == 0:
                        res = res * v[1]
                        break

            elif len(op.atoms(AppliedUndef)):
                # we calculate a new field if we have a fiel to calculate
                name = GetNewName()
                t = GetField(name,1,star=False,sdim=0,extraCoordinates=allcoords )[0]
                res = res * t
                ops[name] = (op,t)
            else:
                name = "S"+GetNewName()
                t = Symbol(name)
                res = res * t
                ops[name] = (op,t)

        elif np.any([ x[0]-term ==0  for x in ops.values() ] ):
            for _k,v in ops.items():
                if v[0] - term == 0:
                    return  v[1]

        elif len(term.atoms()) == 1:
            return term

        elif np.any([ term.has(x) for x in unkownFields ]):
            return term
        elif len(term.atoms(AppliedUndef)):
            # we calculate a new field if we have a fiel to calculate
            name = GetNewName()
            t = GetField(name,1,star=False,sdim=0,extraCoordinates=allcoords )[0]
            res = res * t
            ops[name] = (op,t)
        else  :
            name = "TS"+GetNewName()
            t = Symbol(name)
            ops[name] = (term,t)
            return  t
        return res


    exp = exp.expand()
    newExpr = 0


    if exp.func == Add:
        print(exp)
        for monom in exp.args:
            newMonom = CompactTerm(monom)
            newExpr = newExpr + newMonom

    else:
        newExpr = CompactTerm(exp)


    return newExpr, ops


def SymWeakToNumWeakSeparated(exp,separation=None):

    exp = exp.expand()

    if separation is None:
        res = PyWeakForm()
    else:
        res = [PyWeakForm() for s in separation]

    try:
        if exp.shape[0] == 1 and exp.shape[1] == 1:
            exp = exp[0,0]
    except:
        pass

    def AddTerm(forms,monos):
        if separation is None:
            forms.AddTerm(monos)
        else:
            for f,m in zip(forms,monos):
                f.AddTerm(m)

    if exp.func == Add:
        for monom in exp.args:
            monom = SymWeakMonomToNumWeakMono(monom,separation=separation)
            AddTerm(res,monom)
    else:
        monom = SymWeakMonomToNumWeakMono(exp,separation=separation)
        AddTerm(res,monom)

    return res



def SymWeakMonomToNumWeakMono(exp,separation):

    if separation is None:
        res = PyWeakMonom()
    else:
        res = [PyWeakMonom() for s in separation]

    def AddProd(monoms,terms):
        if separation == None:
            monoms.AddProd(terms)
        else:
            for m,t in zip(monoms,terms):
                if t is not None:
                    m.AddProd(t)


    if exp.is_Number:
        if separation is None:
            res.prefactor = float(exp)
        else:
            res[0].prefactor = float(exp)
        return res

    if exp.func == Mul:
        for arg in exp.args:
            if arg.is_Number:
                if separation is None:
                    res.prefactor = float(arg)
                else:
                    res[0].prefactor = float(arg)
                continue

            if isinstance(arg,Pow):
                term = ConverTermToProd(arg.args[0],separation=separation)
                if term is None: #pragma no cover
                    #print(type(arg.args[0]))
                    #print(arg.args[0])
                    raise( Exception("Unable to treat term " + str(arg.args[0]) ))
                for _i in range(arg.args[1]):
                    #res.AddProd(term)
                    AddProd(res,term)
                continue

            term = ConverTermToProd(arg,separation=separation)
            if term is not None:
                #res.prod.append(term)
                #res.AddProd(term)
                AddProd(res,term)
                continue


            print(type(arg))  #pragma no cover
            print(arg)        #pragma no cover
            raise RuntimeError() #pragma no cover

        return res
    else:
        #only one term no product
        term = ConverTermToProd(exp,separation=separation)
        if term is not None:
            res.AddProd(term)
        return res

def ConverTermToProd(arg,separation=None):
    def Return(data):
        if separation==None:
            return data
        else:
            res = [None]*len(separation)
            for cpt,s in enumerate(separation):
                if cpt == len(separation) - 1:
                    res[-1] = data
                else:
                    for coord in s:
                        if arg.diff(coord) != 0:
                            res[cpt] = data
                            return res
            return res

    if isinstance(arg,Symbol):
        t = PyWeakTerm()
        t.constant = True
        t.derDegree = 0
        t.fieldName = str(arg)
        return Return(t)

    from sympy.core.function import Derivative
    if type(arg) == Derivative:
        t = PyWeakTerm()

        t.fieldName = str(arg.args[0].func)
        #sympy 1.2
        t.derCoordName = str(arg.args[1][0])
        t.derDegree = arg.args[1][1]

        sn = []
        for i in range(0,len(arg.args[0].args)):
            sn.append(str(arg.args[0].args[i] ) )

        t.derCoordIndex_ =  sn.index(t.derCoordName)
        return Return(t)

    if isinstance(arg,Function):
        t = PyWeakTerm()
        N = GetNormal(3)
        #print(str(arg.func)+"* * * * * * ")
        #print(arg.func)
        #print (N)
        #print ([arg == nc for nc in N])
        if np.any([arg == nc for nc in N]):
            t.normal = True
            t.derDegree = int(str(arg.func).split("_")[1])
        else:
            t.derDegree = 0

        t.fieldName = str(arg.func)
        return Return(t)
    print("arg")
    print(arg)
    raise RuntimeError() #pragma no cover


def CheckIntegrity(GUI=False):

    param1 = Symbol("p1")

    separation = [space[0:2],[param1]]
    t = GetSeparatedField('t',1,separation=separation)
    tt = GetSeparatedField('t',1,star=True,separation=separation)
    p = GetSeparatedField('p',1,star=False,separation=separation)

    print(t)
    print(tt)

    sweak = 2*p**2*t*tt.diff(Symbol('x')) + (tt*t).diff(Symbol('x')).diff(param1).diff(param1)

    print("AAAAAAAA")

    print(sweak)

    #from BasicTools.FE.WeakForms import NumericalWeakForm as NWF
    nweak = SymWeakToNumWeakSeparated(sweak)
    print(nweak)

    singleweak = SymWeakToNumWeakSeparated(param1)
    from BasicTools.FE.SymWeakForm import GetNormal


    print("BBBBBBBB")

    duoWeak = GetNormal(3)[0]*param1
    print(duoWeak)
    singleweak = SymWeakToNumWeakSeparated(duoWeak)
    print(singleweak)

    print("CCCCCCC")

    nwfs = SymWeakToNumWeakSeparated(sweak,separation=separation)

    for nwf in nwfs:
        print(nwf)
        print("----------")

    print("Test of SeparateSymExpr " + str(separation))
    exprs = []
    x = space[0]
    y = space[1]
    T = Symbol("T")
    A = Symbol("A")
    B = Symbol("B")
    p1 = Symbol("p1")
    u = GetField("u",1,star=False,sdim=2)[0]
    ut = GetField("ut",1,star=True,sdim=2)[0]
    rho = GetField("rho",1,star=False,sdim=2)[0]
    normal = GetNormal(2)
    exprs.append((2*T)/T)
    exprs.append((2*T)/T+T)
    exprs.append(2*T)
    exprs.append(3*T**2)
    exprs.append(T**2)
    exprs.append(4*T**2+5*A )
    exprs.append(6*T**2+7*A+8*x*y*p1 )

    exprs.append(2*normal[0]+8*x*y*p1+8*y*p1)
    exprs.append(u*2*rho*A+ut*2*A*rho+rho*ut)
    exprs.append(sweak)
    exprs.append(sweak[0]+1)
    exprs.append(u)

    exprs.append(3*A*B*u +3*A*B*u*ut+u.diff(space[0])*ut.diff(space[0])*rho.diff(space[0])+3*u*ut + ut/rho)
    for exp in exprs:
        print("----------")
        print(exp)
        print ("")
        print( SeparateSymExpr(exp,separation) )

    print("----------------Test CompactSymWeak--------------")
    separation = [space[0:2],[param1]]
    for exp in exprs:
        print("----------")
        print(exp)
        newExpr, ops = CompactSymWeak(exp,[u,ut],separation=separation)

        print(newExpr)
        print(ops)
    print("----------------Test SubsSeparatedRepresentation--------------")
    print(separation)

    for exp in exprs:
        print("----------")
        print(exp)
        res = SubsSeparatedRepresentation(exp,separation=separation)
        print(res)
    print("+++++++++++++++++++++++++")

    print(SubsSeparatedRepresentation(u.diff(space[0]),separation=separation).doit())
    return "ok"

if __name__ == '__main__':
    print(CheckIntegrity(True))# pragma: no cover
