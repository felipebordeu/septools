

def WritePxdmf(filename,meshes,fields,binary=True):
    from BasicTools.IO.XdmfWriter import XdmfWriter
    writer = XdmfWriter()
    writer.SetFileName(filename)
    writer.SetXmlSizeLimit(0)
    writer.SetBinary(binary)
    writer.SetParafac(True)
    writer.Open()

    for ipar in range(len(meshes)):
        pointFields = []
        pointFieldsNames = []
        mesh = meshes[ipar]
        for field in fields:
            fname = field.GetName()
            fpar = field.GetPartition(ipar)
            pointFields.extend( [fpar.GetMode(f) for f in range(fpar.GetNumberOfModes()) ] )
            pointFieldsNames.extend(["{}_{}".format(fname,f) for f in  range(fpar.GetNumberOfModes()) ])

        writer.Write(mesh,
                     PointFields = pointFields,
                     PointFieldsNames=pointFieldsNames)
    writer.Close()

def CheckIntegrity(GUI=False):
    return "ok"

if __name__ == '__main__':
    print(CheckIntegrity(True))# pragma: no cover