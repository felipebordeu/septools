# -*- coding: utf-8 -*-

import numpy as np
from scipy.sparse import coo_matrix

from BasicTools.Helpers.BaseOutputObject import BaseOutputObject as BOO,froze_it

import BasicTools.Containers.ElementNames as EN

from BasicTools.FE.Spaces.FESpaces import LagrangeSpaceGeo
from BasicTools.FE.SymWeakForm import testcharacter
#from BasicTools.FE.Fields.FEField import FEField
from SeparatedTools.MultiModeFEField import MultiModeFEField

@froze_it
class MonoElementsIntegral(BOO):
    """
    Class to assembly a formulation (weak form) into a matrix or a vector

    * unkownDofsOffset : offset for the unkowns dofs
    * __ufs__          : unkown fields
    * testDofsOffset   : offset for the test dofs
    * __tfs__          : unkown dofs

    * totalTestDofs    : Total number fo test dofs    (computed Automaticaly)
    * totalUnkownDofs  : Total number fo unkown dofs  (computed Automaticaly)
    * geoSpace         : Geometry aproximation space  (computed Automaticaly)

    * __efs__          : Extra Fields
    * __cfs__          : (dic(str:float) ) Constants
    * integrationRule  : integration rule for the integratoin
    * onlyEvaluation   : To force the integrator to not multiply by the detJac

    * numberOfVIJ = 0
    * F                : rhs vector
    * vK, iK, jK       : Vectors containing the values and indices for the entries
                         of the operator
    * totalvijcpt      : Number of non zero entries in the self.vK iK ant jK vector
    * maxNumberOfTerms : maximal number of terms in a monom (computed Automaticaly)
    * maxNumberOfElementVIJ : (computed Automaticaly)
    * hasnormal        : (computed Automaticaly)
    * __usedSpaces__
    * geoSpaceNumber
    """
    def __init__(self):
        super(MonoElementsIntegral,self).__init__()
        self.unkownDofsOffset = None
        self.__ufs__ = None

        self.testDofsOffset = None
        self.__tfs__ = None

        self.totalTestDofs = 0
        self.totalUnkownDofs= 0
        self.geoSpace = None
        self.__efs__ = None
        self.__ipefs__ = None
        self.__cfs__ = {}
        self.integrationRule = None
        self.onlyEvaluation = False
        """
        For the evaluation we only add the constribution without doing the integration
        the user is responsible of dividing by the mass matrix to get the correct values
        also the user can use a discontinues field to generate element surface stress (for example)
        """
        self.numberOfVIJ = 0

        self.F  = None
        self.vK = None
        self.iK = None
        self.jK = None
        self.totalvijcpt = 0
        self.maxNumberOfTerms = 0
        self.maxNumberOfElementVIJ = 0
        self.hasnormal = False
        self.__usedSpaces__ = None
        self.__usedNumbering__ = None
        self.__usedValues__ = None
        self.__usedValuesAtIP__ = None
        self.geoSpaceNumber = 0

        # internal variables dependent on the courrent element type been treatd
        # (internal use only )
        self.localSpaces = None
        self.localNumbering = None
        self.NumberOfShapeFunctionForEachSpace = None
        self.p = None
        self.w = None
        self.nodes = None
        self.connectivity = None
        self.internalWeakForm = None

    def SetUnkownFields(self,ufs):
        """
        Set the fields used for the unkown space

        ufs : list(FEField) list of fields
        """
        self.__ufs__ = ufs

        self.unkownDofsOffset = np.zeros(len(ufs),dtype=int)
        self.totalUnkownDofs = 0
        cpt = 0
        for uf in ufs:
          self.unkownDofsOffset[cpt] = self.totalUnkownDofs
          self.totalUnkownDofs += uf.numbering["size"]
          cpt += 1

    def SetTestFields(self,tfs=None):
      """
      Set the fields used for the test space

      tfs : list(FEField) list of fields
      if tfs is none then the unkown fields are used (Galerkin projection)
      """
      if tfs is None:
         tfs = []
         for f in self.__ufs__:
            obj = type(f)(name=f.name+testcharacter,mesh=f.mesh,space=f.space,numbering=f.numbering,data=f.GetHeavyData())
            tfs.append(obj)

      self.__tfs__ = tfs

      self.testDofsOffset = np.zeros(len(tfs),dtype=int)
      self.totalTestDofs = 0
      cpt =0
      for tf in tfs:
          self.testDofsOffset[cpt] = self.totalTestDofs
          self.totalTestDofs += tf.numbering["size"]
          cpt += 1

    def SetExtraFields(self,efs):
        """
        Set the extra fields used in the weak formulation

        efs : list(FEField or IPField) list of fields

        """
        self.__efs__ = []
        self.__ipefs__ = []
        for ef in efs:
            if isinstance(ef,MultiModeFEField):
                self.__efs__.append(ef)
            else:
                self.__ipefs__.append(ef)

    def SetConstants(self,cfs):
        """
        Set The constants used in the weak formulation

        cfs : dic(str:float) constants dictionary
        """
        self.__cfs__ = cfs

    def ComputeNumberOfVIJ(self,mesh,elementFilter):
        """
        Compute and return the number triplets to be calculated during integration
        """
        self.numberOfVIJ = 0
        self.maxNumberOfElementVIJ = 0
        for name, _data,ids in elementFilter:
            if len(ids) == 0:
                continue
            numberOfUsedElements = len(ids)

            us = np.sum([f.space[name].GetNumberOfShapeFunctions() for f in self.__ufs__] )
            ts = np.sum([f.space[name].GetNumberOfShapeFunctions() for f in self.__tfs__ ] )

            self.maxNumberOfElementVIJ = max(self.maxNumberOfElementVIJ,numberOfUsedElements*(us*ts)*len(self.integrationRule[name][1]))
            self.numberOfVIJ += numberOfUsedElements*(us*ts)*len(self.integrationRule[name][1])
        return self.numberOfVIJ

    def SetIntegrationRule(self,itegrationRuleOrName=None):
        """
        Function to set the integration Rule
        """
        if itegrationRuleOrName is None :
            from BasicTools.FE.IntegrationsRules import LagrangeP1
            self.integrationRule = LagrangeP1
        elif  type(itegrationRuleOrName) == dict :
            self.integrationRule = itegrationRuleOrName
        elif  type(itegrationRuleOrName) == str :
            from BasicTools.FE.IntegrationsRules import IntegrationRulesAlmanac
            self.integrationRule = IntegrationRulesAlmanac[itegrationRuleOrName]
        else:
            raise(Exception("Error seting the integration rule.."))

    def PrepareFastIntegration(self,mesh,wform,vK,iK,jK,cpt,F):
      """
      Function to prepare the integration procedure, this function checks:
          - if the weak form needs the normal at each integration point
          - prepare the fields to be used
          - fills each term in the weak formulation with the data about the
            fields for fast acces

      mesh : a mesh
      wform: the weak form to be integrated
      vK,iK,jK = the vectors to store the calculated values for the K op
      cpt
      """



      ##we modified the internal structure (varialbe ending with _) for fast access
      self.hasnormal = False
      for monom in wform:
         for term in monom:
             if "Normal" in term.fieldName:
                 self.hasnormal = True
                 break
         if self.hasnormal == True:
             break

      constantNames = []
      for x in self.__cfs__:
          constantNames.append(x)

      ## spaces treatement
      spacesId = {}
      spacesNames = {}
      spacesId[id(self.geoSpace)] = self.geoSpace
      spacesNames["Geometry_Space_internal"] = id(self.geoSpace)

      for uf in self.__ufs__:
          spacesId[id(uf.space)] = uf.space
          spacesNames[uf.name] = id(uf.space)
      for tf in self.__tfs__:
          spacesId[id(tf.space)] = tf.space
          spacesNames[tf.name] = id(tf.space)
      for ef in self.__efs__:
          spacesId[id(ef.space)] = ef.space
          spacesNames[ef.name] = id(ef.space)

      sId = list(spacesId.keys())
      self.__usedSpaces__ =  [ spacesId[k] for k in sId]
      self.geoSpaceNumber = sId.index(spacesNames["Geometry_Space_internal"])
      spacesNames = { sn:sId.index(spacesNames[sn]) for sn in spacesNames }

      # Numbering treatement
      numberingId = {}
      numberingNames = {}
      for uf in self.__ufs__:
          numberingId[id(uf.numbering)] = uf.numbering
          numberingNames[uf.name] = id(uf.numbering)
      for tf in self.__tfs__:
          numberingId[id(tf.numbering)] = tf.numbering
          numberingNames[tf.name] = id(tf.numbering)
      for ef in self.__efs__:
          numberingId[id(ef.numbering)] = ef.numbering
          numberingNames[ef.name] = id(ef.numbering)

      nId = list(numberingId.keys())
      self.__usedNumbering__ =  [ numberingId[k] for k in nId]

      numberingNames = { sn:nId.index(numberingNames[sn]) for sn in numberingNames}

      # Values treatement
      valuesId = {}
      valuesNames = {}
      for ef in self.__efs__:
          data = ef.GetHeavyData()
          valuesId[id(data)] = data
          valuesNames[ef.name] = id(data)

      vId = list(valuesId.keys())
      self.__usedValues__ =  [ valuesId[k] for k in vId]

      valuesNames = { sn:vId.index(valuesNames[sn]) for sn in valuesNames}

      self.maxNumberOfTerms = 0
      for monom in wform:
        self.maxNumberOfTerms = max(self.maxNumberOfTerms, monom.GetNumberOfProds())
        for term in monom:

            if "Normal" in term.fieldName :
                term.internalType = term.EnumNormal
            elif term.constant:
                if term.fieldName in constantNames:
                    term.valuesIndex_ = constantNames.index(term.fieldName)
                    term.internalType = term.EnumConstant
                elif term.fieldName in spacesNames:
                    print("Warning: constant '" +str(term.fieldName) + "'  not found in constants ")
                    print("searching in extra fields")
                    term.spaceIndex_= spacesNames[term.fieldName]
                    term.numberingIndex_= numberingNames[term.fieldName]
                    term.valuesIndex_= valuesNames[term.fieldName]
                    term.internalType = term.EnumExtraField
                elif term.fieldName in  term.fieldName in [f.name for f in self.__ipefs__]:
                    print("Warning: constant '" +str(term.fieldName) + "'  not found in constants ")
                    print("searching in ip fields")
                    term.valuesIndex_= [ef.name for ef in  self.__ipefs__ ].index(term.fieldName)
                    term.internalType = term.EnumExtraIPField

            elif term.fieldName in [f.name for f in self.__ufs__] :
                term.spaceIndex_= spacesNames[term.fieldName]
                term.numberingIndex_= numberingNames[term.fieldName]
                #used for the offset
                term.valuesIndex_= [uf.name for uf in  self.__ufs__ ].index(term.fieldName)

                term.internalType = term.EnumUnknownField
            elif term.fieldName in [f.name for f in self.__tfs__]:
                term.spaceIndex_= spacesNames[term.fieldName]
                term.numberingIndex_= numberingNames[term.fieldName]
                #term.valuesIndex_= valuesNames[term.fieldName]
                term.valuesIndex_= [uf.name for uf in  self.__tfs__ ].index(term.fieldName)

                term.internalType = term.EnumTestField
            elif term.fieldName in [f.name for f in self.__efs__]:
                term.spaceIndex_= spacesNames[term.fieldName]
                term.numberingIndex_= numberingNames[term.fieldName]
                term.valuesIndex_= valuesNames[term.fieldName]
                term.internalType = term.EnumExtraField
            elif term.fieldName in [f.name for f in self.__ipefs__]:
                term.valuesIndex_= [ef.name for ef in  self.__ipefs__ ].index(term.fieldName)
                term.internalType = term.EnumExtraIPField
            else :
                term.internalType = term.EnumError
                raise(Exception("Term " +str(term.fieldName) + " not found in the database " ))

      self.internalWeakForm = type(wform)()

      from SeparatedTools.MiscTools import Expand

      for monom in wform:
#        self.maxNumberOfTerms = max(self.maxNumberOfTerms, monom.GetNumberOfProds())

        modes = []
        for term in monom:
            if term.internalType == term.EnumExtraField :
                modes.append(self.__efs__[term.valuesIndex_].GetNumberOfModes())
            elif term.internalType == term.EnumExtraIPField:
                modes.append(self.__ipefs__[term.valuesIndex_].GetNumberOfModes())
            else:
                modes.append(None)

        for cpt in Expand(modes):
            newMonom = monom.copy()

            for term,mode in zip(newMonom,cpt):
                if mode is None:
                    continue
                else:
                    term.modeIndex_ = mode
            self.internalWeakForm.AddTerm(newMonom)

      numberOfOps = 0
      for monom in self.internalWeakForm:
         self.maxNumberOfTerms = max(self.maxNumberOfTerms, monom.GetNumberOfProds())
         for term in monom:
            if term.fieldName in [f.name for f in self.__ufs__]:
                numberOfOps +=1
                break




      numberOfFF = self.internalWeakForm.GetNumberOfTerms()-numberOfOps
      print()
      self.vK = [None]*numberOfOps
      self.iK = [None]*numberOfOps
      self.jK = [None]*numberOfOps
      self.totalvijcpt = [0]*numberOfOps
      self.F = np.zeros((len(F),numberOfFF))

      for i in range(numberOfOps):
          self.vK[i] = np.zeros_like(vK)
          self.iK[i] = np.zeros_like(iK)
          self.jK[i] = np.zeros_like(jK)

      self.SetPoints(mesh.nodes)

    def SetPoints(self,nodes):
        """
        ## from https://github.com/cython/cython/wiki/tutorials-NumpyPointerToC

        multiply (arr, value)

        Takes a numpy arry as input, and multiplies each elemetn by value, in place

        param: array -- a 2-d numpy array of np.float64

        """
        self.nodes = nodes

        return None

    def SetOnlyEvaluation(self,onlyEvaluation= True):
      """
      To activate the Only Evaluation functionality
          For the evaluation we only add the constribution without doing the integration (multiplication by the detjac )
          the user is responsible of dividing by the mass matrix to get the correct values
          . Ffr example  the user can use a discontinues field to generate element surface stress
      """
      self.onlyEvaluation = onlyEvaluation

    def ActivateElementType(self,domain):
        """
        Function to prepared the integration for a type of element
        domain : (ElementsContainer)

        """

        self.localNumbering = []
        for numbering in self.__usedNumbering__:
            self.localNumbering.append( numbering.get(domain.elementType,None) )


        self.p, self.w = self.integrationRule[domain.elementType]

        self.geoSpace = LagrangeSpaceGeo[domain.elementType]
        self.geoSpace.SetIntegrationRule(self.p,self.w)

        self.NumberOfShapeFunctionForEachSpace = np.zeros(len(self.__usedSpaces__), dtype=int)

        cpt = 0
        self.localSpaces = list()
        for space in self.__usedSpaces__:
            if space is None :
                self.NumberOfShapeFunctionForEachSpace[cpt] = 0
                self.localSpaces.append(None)
            else:
                self.localSpaces.append(space[domain.elementType])
                space[domain.elementType].SetIntegrationRule(self.p,self.w)
                self.NumberOfShapeFunctionForEachSpace[cpt] = space[domain.elementType].GetNumberOfShapeFunctions()
            cpt += 1

        self.connectivity = domain.connectivity

        self.__usedValuesAtIP__ = [ipef.GetHeavyData()[domain.elementType] for ipef in self.__ipefs__ ]

    def Integrate(self,wform,idstotreat):
        """
        Main function to execute the integration
        wform: (PyWeakForm) Python Or C++ version of the weak form to be integrated
        idstotreat:  list like (int) ids of the element to treat
        """
        constantsNumerical = np.empty(len(self.__cfs__))
        cpt =0
        for x in self.__cfs__:
            constantsNumerical[cpt] = self.__cfs__[x]
            cpt += 1

        NumberOfIntegrationPoints = len(self.w)

        ev = np.empty(self.maxNumberOfElementVIJ*NumberOfIntegrationPoints,dtype=np.float)
        ei = np.empty(self.maxNumberOfElementVIJ*NumberOfIntegrationPoints,dtype=np.int)
        ej = np.empty(self.maxNumberOfElementVIJ*NumberOfIntegrationPoints,dtype=np.int)

        numberOfFields = len(self.__usedSpaces__)

        BxByBzI = [None] *numberOfFields
        NxNyNzI = [None] *numberOfFields

        for n in idstotreat:

            xcoor = self.nodes[self.connectivity[n],:]


            for ip in range(NumberOfIntegrationPoints):
                """ we recover the jacobian matrix """
                Jack, Jdet, Jinv = self.geoSpace.GetJackAndDetI(ip,xcoor)

                for i in range(numberOfFields):
                     if self.localSpaces[i] is not None:
                         NxNyNzI[i] = self.localSpaces[i].valN[ip]

                         BxByBzI[i] = Jinv(self.localSpaces[i].valdphidxi[ip])

                if self.hasnormal:
                    normal = self.geoSpace.GetNormal(Jack)

                FFcpt =0
                Opscpt =0
                for monom in self.internalWeakForm:
                    fillcpt =0

                    factor = monom.prefactor
                    if self.onlyEvaluation :
                        # For the evaluation we only add the constribution without doing the integration
                        # the user is responsible of dividing by the mass matrix to get the correct values
                        # also the user can use a discontinues field to generate element surface stress (for example)
                        pass
                    else:
                        # for the integration we multiply by the deteminant of the jac
                        factor *= Jdet
                        factor *= self.w[ip]

                    hasright = False

                    for term in monom:

                        if term.internalType == term.EnumNormal :
                            factor *= normal[term.derDegree]
                            continue
                        elif  term.internalType == term.EnumConstant :
                            factor *= constantsNumerical[term.valuesIndex_]
                            continue
                        elif  term.internalType == term.EnumUnknownField :
                            if term.derDegree == 1:
                                right = BxByBzI[term.spaceIndex_][[term.derCoordIndex_],:]
                            else:
                                right = np.array([NxNyNzI[term.spaceIndex_],])
                            rightNumbering = self.localNumbering[term.numberingIndex_][n,:] + self.unkownDofsOffset[term.valuesIndex_]
                            hasright = True
                            l2 = self.NumberOfShapeFunctionForEachSpace[term.spaceIndex_]
                            continue
                        elif  term.internalType == term.EnumTestField :
                            if term.derDegree == 1:
                                left = BxByBzI[term.spaceIndex_][term.derCoordIndex_]
                            else:
                                left = NxNyNzI[term.spaceIndex_]
                            leftNumbering = self.localNumbering[term.numberingIndex_][n,:] + self.testDofsOffset[term.valuesIndex_]
                            l1 = self.NumberOfShapeFunctionForEachSpace[term.spaceIndex_]
                            continue
                        elif term.internalType == term.EnumExtraField :

                            if term.derDegree == 1:
                                func = BxByBzI[term.spaceIndex_][term.derCoordIndex_]
                            else:
                                func = NxNyNzI[term.spaceIndex_]
                            centerNumbering = self.localNumbering[term.numberingIndex_][n,:]
                            vals = self.__usedValues__[term.valuesIndex_][term.modeIndex_,centerNumbering]
                            factor *= np.dot(func,vals)

                            continue
                        elif term.internalType == term.EnumExtraIPField :
                            if term.derDegree == 1:
                                raise(Exception("Integration point field cant be derivated"))
                            val = self.__usedValuesAtIP__[term.valuesIndex_][term.modeIndex_,n,ip]
                            factor *= val
                        else :
                            raise(Exception("Cant treat term " + str(term.fieldName)))

                    #if factor == 0:
                    #    continue



                    if hasright:
                        l = l1*l2

                        l2cpt = fillcpt
                        for i in range(l1):
                            for j in range(l2) :
                                ev[l2cpt] =  left[i]*right[0,j]*factor
                                l2cpt +=1

                        l2cpt = fillcpt
                        for i in range(l1):
                            for j in range(l2) :
                              ej[l2cpt] = rightNumbering[j]
                              l2cpt += 1

                        l2cpt = fillcpt
                        for j in range(l2) :
                             for i in range(l1):
                                  ei[l2cpt] = leftNumbering[j]
                                  l2cpt += 1
                        fillcpt += l
                    else:
                        for i in range(l1):
                          self.F[leftNumbering[i],FFcpt] += left[i]*factor

                        FFcpt +=1


                    if fillcpt:
                        data = coo_matrix((ev[:fillcpt], (ei[:fillcpt],ej[:fillcpt])), shape=( self.totalTestDofs,self.totalUnkownDofs))
                        data.sum_duplicates()
                        start = self.totalvijcpt[Opscpt]
                        stop = start+len(data.data)
                        #print("-----------------------")
                        #print(data.data)
                        #print(start)
                        #print(stop)
                        #print(self.vK[Opscpt])
                        self.vK[Opscpt][start:stop] = data.data
                        self.iK[Opscpt][start:stop] = data.row
                        self.jK[Opscpt][start:stop] = data.col
                        self.totalvijcpt[Opscpt] += len(data.data)

                    if hasright:
                        Opscpt +=1
    def GenerateOutput(self):

        from SeparatedTools.MultiModeOperator import MultiModeOperator
        from SeparatedTools.MultiModeFEField import MultiModeFEField
        KK = MultiModeOperator()
        nbops = len(self.vK)
        KK.SetNumberOfModes(nbops)
        for i in range(nbops ):
            numberOfUsedvij = self.GetNumberOfUsedIvij()[i]
            data = (self.vK[i][0:numberOfUsedvij], (self.iK[i][0:numberOfUsedvij],self.jK[i][0:numberOfUsedvij]))
            K = coo_matrix(data, shape=(self.GetTotalTestDofs(), self.GetTotalUnkownDofs())).tocsr()
            KK.data[i] = K

        FF = MultiModeFEField()
        FF.internaldata = self.F.T
        print(FF.internaldata.shape)
        print("-----------------")

        return KK, FF

    def GetNumberOfUsedIvij(self):
        """
        Return number of non zero values in the vectors vK,iK,jK
        """
        return self.totalvijcpt

    def AddToNumbefOfUsedIvij(self,data):
        self.totalvijcpt += data

    def GetTotalTestDofs(self):
        """
        Return the number of dofs of the test space (number of rows of the K matrix)
        """
        return self.totalTestDofs

    def GetTotalUnkownDofs(self):
        """
        Return the number of dofs of the Unkown space (number of cols of the K matrix)
        """
        return self.totalUnkownDofs



def SeparatedIntegrateGeneral( meshes, wform,unkownFields, constants=None, fields=None , testFields=None, integrationRuleName=None,onlyEvaluation=None, elementFilter=None,separation=None):

    # pretreat
    if not isinstance(wform,list):
        #in the case we have I *normal* weak form (not separated)

        # first we compact the weak form to minimise the numer of term is the lin sys

        unkAndTestFields = unkownFields[:]
        if testFields is not None:
            unkAndTestFields.extend(testFields)

        from SeparatedTools.SeparatedWeakForm import CompactSymWeak
        compactWeak, ops = CompactSymWeak(wform,unkAndTestFields,separation=separation)

        # We transfer all fields to the integration points
        from SeparatedTools.FieldTools import GetIPFieldFromField
        if fields is not None:
            newFields = []
            for f in fields:
                nf = GetIPFieldFromField(f,ruleName=integrationRuleName,elementFilter=elementFilter,der=None)
                newFields.append(nf)

        from SeparatedTools.MiscTools import ComputeAuxiliaryFields
        # we compute the value of the axiliary fields at the integration points
        fields = ComputeAuxiliaryFields(newFields,ops,constants=constants, elementFilter=elementFilter)

        # inject the separation into the new weakform
        from SeparatedTools.SeparatedWeakForm import SubsSeparatedRepresentation
        symsepwform = SubsSeparatedRepresentation(compactWeak,separation=separation)

        # convertion to numericalweakform
        from SeparatedTools.SeparatedWeakForm import SymWeakToNumWeakSeparated
        wform = SymWeakToNumWeakSeparated(symsepwform,separation=separation)


    from SeparatedTools.SeparatedLinearSystem import SeparatedLinearSystem

    res = SeparatedLinearSystem()
    from BasicTools.FE.Integration import IntegrateGeneral
    res.SetNumberOfPartitions(len(meshes))

    for i in range(len(meshes)):
        m = meshes[i]
        w = wform[i]

        if constants is None:
            constants = {}

        if fields is None:
            pfields = []
        else:
            pfields = [field.GetPartition(i) for field in fields]

        punkownFields = [field.GetPartition(i) for field in unkownFields]

        if testFields is None:
            ptestFields = None
        else:
            ptestFields = [field.GetPartition(i) for field in testFields]

        if integrationRuleName is None:
            pintegrationRuleName = None
        else:
            pintegrationRuleName = integrationRuleName[i]

        if onlyEvaluation is None:
            ponlyEvaluation = None
        else:
            ponlyEvaluation = onlyEvaluation[i]

        if elementFilter is None:
            pelementFilter= None
        else:
            pelementFilter = elementFilter[i]

        integrator = MonoElementsIntegral()

        print("Integration of partition " + str(i))

        IntegrateGeneral( mesh    = m,
                                  wform   = w,
                                  constants = constants,
                                  fields= pfields,
                                  unkownFields = punkownFields,
                                  testFields=ptestFields,
                                  integrationRuleName=pintegrationRuleName,
                                  onlyEvaluation=ponlyEvaluation,
                                  elementFilter=pelementFilter ,
                                  userIntegrator= integrator )
        KK, FF = integrator.GenerateOutput()

        res.KK.SetPartition(i,KK)
        res.FF.SetPartition(i,FF)
        cpt = np.sum([pf.numbering["size"] for pf in punkownFields])
        res.LinearSolvers[i].constraints.SetNumberOfDofs(cpt)
        res.meshes.append(m)

    res.unkownFields = unkownFields
    return res

def CheckIntegrity():

    ########################## Symbolic part ########################################
    from SeparatedTools.SeparatedWeakForm import GetSeparatedField
    from BasicTools.FE.SymPhysics import ThermalPhysics
    from BasicTools.FE.SymWeakForm import GetField,space
    from sympy import Symbol

    separation = [space[0:1],space[1:2]]
    print(separation)
    thermalPhysics = ThermalPhysics()
    thermalPhysics.spaceDimension = 2
    thermalPhysics.primalUnknown = GetSeparatedField(name = thermalPhysics.thermalPrimalName[0],
                                                     size = thermalPhysics.thermalPrimalName[1],
                                                     star = False ,
                                                     separation = separation)

    thermalPhysics.primalTest = GetSeparatedField(name = thermalPhysics.thermalPrimalName[0],
                                                     size = thermalPhysics.thermalPrimalName[1],
                                                     star = True ,
                                                     separation = separation)



    alphaGlobal = Symbol('AlphaG')
    alphaLocalIp = Symbol('alphaLIP')
    alphaLocalFE = Symbol('AlphaL')

 #   alphaLocal  = GetSeparatedField(name = "AlphaL",
 #                                  size = 1,
 #                                  star = False ,
 #                                  separation = separation)[0]

    weakForm = thermalPhysics.GetBulkFormulation(alphaGlobal*alphaLocalFE*alphaLocalIp)

    from SeparatedTools.SeparatedWeakForm import SymWeakToNumWeakSeparated
    numericalFormulation = SymWeakToNumWeakSeparated(weakForm,separation=separation)

    ################# Numerical part  ##########################
    Lx = 2
    Ly = 1

    alphaGlobal_Num = 2

    #epsion = 1e-8
    #epsilon_tilde =0
    #Max_terms = 10
    #Max_fp_iter = 20


    Nx = 41
    Ny = 41

    from BasicTools.Containers.UnstructuredMeshCreationTools import CreateUniformMeshOfBars

    Mesh_x = CreateUniformMeshOfBars(0,Lx,Nx)
    Mesh_y = CreateUniformMeshOfBars(0,Ly,Ny)
    Meshes = [Mesh_x, Mesh_y]

    x = Mesh_x.nodes[:,0]
    y = Mesh_y.nodes[:,0]

    # field generations
    from SeparatedTools.MiscTools import PreparePGDComputation
    # basic FE using all the nodes and isoparametric

    T0 = PreparePGDComputation("t0",Meshes)[0]
    T0.Allocate(1,val=0)

    alphaLocal_Num = T0.CopyStructure()
    alphaLocal_Num.SetName("AlphaL")
    alphaLocal_Num.Allocate(1,val=0.5)

    # Because the numbering is computed using connectivity we can do
    #A priori terms imposing the Dirichlet BC (Eq. (2.58))
    GX = (Lx-x)/Lx
    GY = y*(Ly-y)

    T0.GetPartition(0).SetMode(0,GX)
    T0.GetPartition(1).SetMode(0,GY)


    FF = T0.CopyStructure(   )
    FF.SetName("FF")
    FF.Allocate(1)
    #Separated representation of the source term (Eq. (2.59))
    FX = -5*np.exp(-10*(x-Lx/2)**2)
    FY = np.exp(-10*(y-Ly/2)**2)

    FF.GetPartition(0).SetMode(0,FX)
    FF.GetPartition(1).SetMode(0,FY)

    #Neumann BC on upper boundary (Eq. (2.57), last line)
    #q = -1
    #TODO

    # Unknown
    t = T0.CopyStructure()
    t.SetName("t")

    ipField = PreparePGDComputation("alphaLIP",Meshes,integrationPointField=True)[0]
    ipField.Allocate(1,val=1)
    ############################################################
    from BasicTools.Containers.Filters import ElementFilter

    elementFilter =[ElementFilter(mesh=mesh,dimensionality=1) for mesh in Meshes]

    separatedSystem = SeparatedIntegrateGeneral( meshes=Meshes,
                                             wform = numericalFormulation,
                                             constants={"AlphaG":alphaGlobal_Num},
                                             fields=[alphaLocal_Num, ipField],
                                             unkownFields=[t],
                                             testFields=None,
                                             integrationRuleName=None,
                                             onlyEvaluation=None,
                                             elementFilter=elementFilter)

    print(separatedSystem)
    print(separatedSystem.KK)
#    import BasicTools.FE.Integration as Integration
#    backup  = Integration.UseCpp
#
#    Integration.UseCpp = False
#    res = "ok"
#    try:
#        from BasicTools.FE.UnstructuredFeaSym import CheckIntegrity as CI
#        res = CI()
#    except:
#        Integration.UseCpp = backup
#        raise
#    Integration.UseCpp = backup
#    return res
    return "ok"
if __name__ == '__main__':# pragma: no cover
    print(CheckIntegrity())
